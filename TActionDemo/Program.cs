﻿using System;
using System.Collections.Generic;

namespace TActionDemo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<string> names = new List<string>();
            names.Add("Bruce");
            names.Add("Alfred");
            names.Add("Tim");
            names.Add("Richard");

            // Display the contents of the list using the Print method.
            names.ForEach(name => Print(name));

            // The following demonstrates the anonymous method feature of C#
            // to display the contents of the list to the console.
            names.ForEach (delegate (string name)
            {
                Console.WriteLine(name);
            }) ; 
         
            void Print(string s)
            {
                Console.WriteLine(s);
            }

            /* This code will produce output similar to the following:
            * Bruce
            * Alfred
            * Tim
            * Richard
            * Bruce
            * Alfred
            * Tim
            * Richard
            */
        }
    }




}
