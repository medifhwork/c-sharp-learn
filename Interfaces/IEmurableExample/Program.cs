﻿using System;
using System.Collections.Generic;

namespace IEmurableExample
{
    class Program
    {
        static void Main(string[] args)
        {
            var Ibn = new Human();
            foreach (var item in Ibn)
            {
                Console.WriteLine("123");
            }
            Console.ReadKey();
        }

        public class Human
        {
            private int _en = 0;

            public int En { get => _en; set => _en = value; }

            //public IEnumerator<int> GetEnumerator()  // общий пример,  в целом досточно 1 метода  
            //{
            //    return null;
            //}

            public IEnumerator<int> GetEnumerator() => null;


        }
    }
}
