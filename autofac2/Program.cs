﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;

namespace autofac2demo
{
    internal class Program
    {  //хранит зависимости и управляет жизненным циклом добавленных компонентов
        private static IContainer Container { get; set; }
        
        //кусок от примера example
        public static void WriteDate()
        {
            //иницилизируем  цикл жизни компонента 
            using (var scope = Container.BeginLifetimeScope())
            {//получить службу для контекста  интерфейса датаврайтер
                var writer = scope.Resolve<IDateWriter>();
             //вызвать реализацию зависимую от контекста 
              writer.WriteDate();                
            }
        }

        static void Main(string[] args)
        {
            var builder = new ContainerBuilder();
            //регаем типы с реализацией в контейнере компонентов
            builder.RegisterType<ConsoleOutput>().As<IOutput>();
            //Позволяет вызывать  реализацию по  конкретному интерфейсу
            builder.RegisterType<TomorrowWriter>().AsSelf().As<IDateWriter>();
            builder.RegisterType<TodayWriter>().As<IDateWriter>();
            Container = builder.Build();
            // используем метод созданный с dependency injection
            //обработает последний зарегистрированный тип если не указать иное, как указать иное
            Console.Write("Выведет последний зареганую реализацию в контейнере - "); 
                WriteDate();

            // Надо полуичть экземпляр требуемого типа из контейнера.
            // будет ошибка если при регистрации не указать AsSelf() тк это будет выступать ключем для поиска конкретной реализации
            var tomorrowWriter = Container.Resolve<TomorrowWriter>();
            Console.Write("Выведет реализацию TomorrowWriter -  "); 
             tomorrowWriter.WriteDate();
            Console.Write("Выведет  последнею зареганую, предыдушая строка не влияет на поведение по умолчанию -  "); 
                WriteDate();
            Console.ReadKey();          
        }
    }

    //определяем  интефейс или базовый классс инстумента
    public interface IService { void DoWrite(); }
    //пишем решение инструмента
    public class SomeType : IService
    {
        void IService.DoWrite()
        {
            Console.WriteLine("пишу работаю как кабанчик");
        }
    }

    public class foo
    {
        public void Dofoo1()
        {
            //Создаем контейнер билдер
            var builder = new ContainerBuilder();
            //добавляем наш конкретный тип в сервис
            builder.RegisterType<SomeType>().As<IService>();
            //регистрируем по интерфейсу типа
            builder.RegisterType<SomeType>().AsSelf().As<IService>();

        }
    }
    #region example
    //обьявляем интерфейс вывода с 1 методом напиши
    public interface IOutput
    {
        void Write(string content);
    }
    //конкретизируем интерфейс до вывода  строки в консоль
    public class ConsoleOutput : IOutput
    {
        public void Write(string content)
        {
            Console.WriteLine(content);
        }
    }
    //об  инт напиши дату календаря
    public interface IDateWriter
    {
        void WriteDate();
    }
    //напиши дату календаря сегодя
    public class TodayWriter : IDateWriter
    {
        private IOutput _output;
        //конструктор с агрегацией  вывода (в качестве инверсии зависимости)
        public TodayWriter(IOutput output)
        {
            this._output = output;
        }

        public void WriteDate()
        {
            this._output.Write(DateTime.Today.ToShortDateString());
        }
    }
    //напиши дату календаря завтра
    public class TomorrowWriter : IDateWriter
    {
        private IOutput _output;
        public TomorrowWriter(IOutput output)
        {
            this._output = output;
        }

        public void WriteDate()
        {
            this._output.Write(DateTime.Today.AddDays(1).ToShortDateString());
        }
    }

    #endregion
}
