﻿using System;

namespace ExeptionsExampl
{
    class Program
    {
        static void Main(string[] args)
        {
            byte n = 0;
            try
            {
                Console.WriteLine("Введите целое число <255: ");
                n = Convert.ToByte(Console.ReadLine());
                Console.WriteLine($"Ввод успешен {n}");
            }
            catch (System.FormatException)
            {

                Console.WriteLine("Ввод не корректен!");
            }
            catch (OverflowException)//блоков может быть сколь угодно, finally может быть опущен, как и catch но не одновременно!
            //в финали заходит в любом случаеи не важно какие ретурны и броски были вызваны в основном блоке кода.
            {
                Console.WriteLine("Out of Range value");
            }
        }
    }
}
