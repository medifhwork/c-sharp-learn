﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ExeptionsInTread
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread otherThread = new Thread(WorkInAnotherThread);
            otherThread.Start();
            for (int i = 0; i < 100; i++)
            {
                Thread.Sleep(10);
                Console.WriteLine(i);
            }
        }

        public static void WorkInAnotherThread()
        {
            for (int i = 0; i < 5; i++)
            {
                Thread.Sleep(10);                
            }
            throw new MyExeptions();
        }
    }

    class MyExeptions: Exception
    {

    }
}
