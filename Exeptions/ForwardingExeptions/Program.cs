﻿using System;

namespace ForwardingExeptions
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string? s = null;
                Console.WriteLine(s.Length);
            }
            catch (Exception e) when (DoSomething(e)){} //sql подобный синтаксис с# 6.0 скорее всего.
                      

        }
        //метод заведомо возврашает лож, но через свой аргумент получает обьект исключения
        //своих грязных целей логирования не известных разработчику ислючений
        private static bool DoSomething(Exception e)
        {
            Console.WriteLine(e.Source);
            Console.WriteLine("azazazazazazaa");
            return false;
        }
    }
}
