﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoqTutorial
{
    public interface ILogWriter
    {
        string GetLogger();
        void SetLogger(string logger);
        void Write(string message);
    }
}
