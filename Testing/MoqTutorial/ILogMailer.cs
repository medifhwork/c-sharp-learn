﻿namespace MoqTutorial
{
    public interface ILogMailer
    {
        void Send(MailMessage mailMessage);
    }
}