﻿using System;

namespace MoqTutorial
{
    internal class SmartLogger
    {
        private ILogWriter writer;
        private ILogMailer mailer;

        public SmartLogger(ILogWriter writer, ILogMailer mailer)
        {
            this.writer = writer;
            this.mailer = mailer;
        }

        public void WriteLine(string value)
        {
            writer.Write(value);
        }

        public void SendMail (MailMessage mail)
        {
            mailer.Send(mail);     
        }
    }
}