﻿using System;

namespace SampleTest_1
{
    public class Calculator
    {
        public int Add(int? a, int? b)
        {
            if (a == null || b ==null) throw new ArgumentNullException("huemoe");
            int? x = a + b;
            return x.Value;
        }
    }

}