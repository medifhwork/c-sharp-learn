﻿using SampleTest_1.Reporter;
using System;
using System.Collections.Generic;
using System.Text;

namespace SampleTest_1.TDDConcept.Reporter
{
    public interface IReportBuilder
    {
        IList<Report> CreateRegularReports();
        SpecialReport CreateSpecialReport();
        AuditorReport CreateAuditorReport();
    }
        
}
