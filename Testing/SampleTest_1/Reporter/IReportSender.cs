﻿using SampleTest_1.Reporter;
using System;
using System.Collections.Generic;
using System.Text;

namespace SampleTest_1.TDDConcept.Reporter
{
    public interface IReportSender
    {
        int SendReport();
        void Send(Report report);
        void Send(SpecialReport specialReport);
        void Send(AuditorReport auditorReport);
    }

   
    

}
