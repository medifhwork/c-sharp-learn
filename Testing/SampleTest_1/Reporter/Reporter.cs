﻿using SampleTest_1.Reporter;
using System;
using System.Collections.Generic;
using System.Text;

namespace SampleTest_1.TDDConcept.Reporter
{
    public class Reporter
    {
        private readonly IReportBuilder reportBuilder;
        private readonly IReportSender reportSender;

        public Reporter(IReportBuilder builder, IReportSender sender) //возможность подключения своих фейков.
        {
            this.reportBuilder = builder;
            this.reportSender = sender;
        }

        public int SendReports()
        {
            //return reportBuilder.CreateRegularReports().Count;
            IList<Report> reports = reportBuilder.CreateRegularReports();

            if (reports.Count == 0)
            {
                reportSender.Send(reportBuilder.CreateSpecialReport());
            }

            for (int i = 0; i < reports.Count; i++)
            {
                if (i % 2 == 0)
                {
                    reportSender.Send(reportBuilder.CreateAuditorReport());
                }
           
                reportSender.Send(reports[i]);
            }

            return reports.Count;
        }

        
    }
}
