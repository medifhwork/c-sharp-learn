﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Login
{
    //выносим тестируемые методы в интерфейс
    internal interface ILastUsernameProvider
    {
        string ReadLastUserName();
        void SaveLastUserName(string userName);
    }

    internal class LastUsernameProvider : ILastUsernameProvider
    {
        // Читаем имя последнего пользователя из некоторого источника данных
        public string ReadLastUserName() { return "Jonh Doe"; }
        // Сохраняем это имя, откуда его можно будет впоследствии прочитать
        public void SaveLastUserName(string userName) { }
    }

    public class LoginViewModel
    {
        public string UserName { get; set; }

        private readonly ILastUsernameProvider _provider;

        // Единственный открытый конструктор создает реальный провайдер
        public LoginViewModel() : this(new LastUsernameProvider())
        {//this ссылается на конструктор в этом же классе с подходящей сигнатурой
        }

        internal LoginViewModel(ILastUsernameProvider provider)
        {
            _provider = provider;
            UserName = _provider.ReadLastUserName();
        }


        public void Login()
        {
            _provider.SaveLastUserName(UserName);
        }
    }


  


}
