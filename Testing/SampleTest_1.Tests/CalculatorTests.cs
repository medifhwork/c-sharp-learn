﻿using NUnit.Framework;
using SampleTest_1;
using System;
using System.Collections.Generic;
using System.Text;

namespace SampleTest_1.Tests
{
    [TestFixture] //This is the attribute that marks a class that contains tests and, optionally, setup or teardown methods.
    public class CalculatorTests
    {

        [SetUp]
        public void SetupCalculator()
        {
            // тут можно установить переменные класса, эти действия будут вызываться перед каждым тестом
        }


        [TearDown]
        public void CleanupValues()
        {
            //выполняется после каждого теста
        }


        [Test]
        public void Add_15Plus65_Expected80()
        {
            //Arrange
            Calculator add = new Calculator();

            //Act
            int result = add.Add(15, 65);

            //Assert
            Assert.That(result, Is.EqualTo(80));
        }

        [Test]
        [TestCase(15, 35, 50)] // запускает тест с этмии параметрами
        [TestCase(-1, 1, 0)]
        [TestCase(0, 0, 0)]
        //[TestCase(20, 80, 110)]
        public void Add_MethodTest_AreEquals(int num1, int num2, int expected)
        {
            Calculator add = new Calculator();
            int result = add.Add(num1, num2);
            Assert.AreEqual(expected, result);
        }

        [Test]
        [Repeat(10)]
        public void Add_15Plus65Repeat10_Expected80()
        {
            Calculator add = new Calculator();
            int result = add.Add(15, 65);
            Assert.That(result, Is.EqualTo(80));
        }

        [Test]
        [MaxTime(1000)]
        public void Add_15Plus65MaxDurations1000ms_Expected80()
        {
            Calculator add = new Calculator();
            int result = add.Add(15, 65);
            Assert.That(result, Is.EqualTo(80));
        }

        //Assert Greater Than Constraint Example
        //Assert.That(result, Is.GreaterThan(10));
        //Assert.That(result, Is.GreaterThanOrEqualTo(10));

        //Assert Ranges Example
        //Assert.That(result, Is.InRange(10, 50));

        //String comparison, Equal Not Equal Constraint Example
        //Assert.That(result, Is.EqualTo("webtrainingroom"));
        //Assert.That(result, Is.Not.EqualTo("training"));
        //Assert.That(result, Is.EqualTo("CaseMatching").IgnoreCase);  


        //Substring Constraint Example
        //Assert.That(result, Does.Contain("training").IgnoreCase);
        //Assert.That(result, Does.Not.Contain("training").IgnoreCase);

        //Regex Constraint Example
        //string result = "training"; 
        //Assert.That(result, Does.Match("t*g"));
        //Assert.That(result, Does.Not.Match("a*x"));

        //Directory and File Constraints, Check if a File or Directory exists.
        //Assert.That(new DirectoryInfo(path), Does.Exist);
        //Assert.That(new DirectoryInfo(path), Does.Not.Exist);
        //Assert.That(new FileInfo(path), Does.Exist);
        //Assert.That(new FileInfo(path), Does.Not.Exist);

        [Test]        
        //[ExpectedException(typeof(Exception))]  deprecated in Nuit3.0
        public void Add_NullPlus1_ExpectANE()
        {
            Calculator add = new Calculator();
            
            var exeption = Assert.Throws<ArgumentNullException>(()=> add.Add(null, 1));
        }

    }
}
