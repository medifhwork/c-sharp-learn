﻿using NUnit.Framework;
using Login;
using System;
using System.Collections.Generic;
using System.Text;

namespace Login
{
    [TestFixture]
    public class LoginViewModelTests
    {
        // Тестовый метод для проверки правильной реализации конструктора вью-модели
        [Test]
        public void TestViewModelConstructor()
        {
            var stub = new LastUsernameProviderStub();
            // "моделируем" внешнее окружение
            stub.UserName = "Jon Skeet"; // Ух-ты!!
            var vm = new LoginViewModel(stub);
            // Проверяем состояние тестируемого класса
            Assert.That(vm.UserName, Is.EqualTo(stub.UserName));
        }
        /*Стабы никогда не применяются в утверждениях, они простые «слуги»,
         * которые лишь моделируют внешнее окружение тестового класса;
         * при этом в утверждениях проверяется состояние именно тестового класса,
         * которое зависит от установленного состояния стаба.
         state-basedtesting */

        internal class LastUsernameProviderStub : ILastUsernameProvider
        {
            // Добавляем публичное поле, для простоты тестирования и 
            // возможности повторного использования этого класса
            public string UserName;

            // Реализация метода очень простая - просто возвращаем UserName
            public string ReadLastUserName()
            {
                return UserName;
            }

            // Этот метод в данном случае вообще не интересен
            public void SaveLastUserName(string userName) { }
        }

        /*У моков же другая роль. Моки «подсовываются» тестируемому объекту,
         * но не для того, чтобы создать требуемое окружение
         * (хотя они могут выполнять и эту роль), а прежде всего для того, 
         * чтобы потом можно было проверить, что тестируемый объект выполнил 
         * требуемые действия. (Именно поэтому такой вид тестирования
         * называется behaviortesting */

        // Мок позволяет проверить, что метод SaveLastUserName был вызван 
        // с определенными параметрами
        internal class LastUsernameProviderMock : ILastUsernameProvider
        {// Теперь в этом поле будет сохранятся имя последнего сохраненного пользователя
            public string SavedUserName;
            // Нам все еще нужно вернуть правильное значение из этого метода,
            // так что наш "мок" также является и "стабом"
            public string ReadLastUserName() { return "Jonh Skeet"; }
            // А вот в этом методе мы сохраним параметр в SavedUserName для 
            public void SaveLastUserName(string userName)
            {
                SavedUserName = userName;
            }
        }

        // Проверяем, что при вызове метода Login будет сохранено имя последнего пользователя
        [Test]
        public void TestLogin()
        {
            var mock = new LastUsernameProviderMock();
            var vm = new LoginViewModel(mock);

            // Изменяем состояние вью-модели путем изменения ее свойства
            vm.UserName = "Bob Martin";
            // А затем вызываем метод Login
            vm.Login();
            // Теперь мы проверяем, что был вызван метод SaveLastUserName
            Assert.That(mock.SavedUserName, Is.EqualTo(vm.UserName));
        }

    }

}