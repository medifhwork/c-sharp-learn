﻿using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using SampleTest_1.Reporter;
using SampleTest_1.TDDConcept.Reporter;


using Xunit;

namespace SampleTest_1.TDDConcept
{

    //Задача состоит из нескольких подзадач:
    //1) написать консольное приложение, которое отправляет отчеты.
    //2) Каждый второй сформированный отчет надо отправлять ещё и аудиторам.
    //3) Если ни одного отчета не сформировано, то отправляем сообщение руководству о том,
    //что отчетов нет.
    //4) После отправки всех отчётов, нужно вывести в консоль количество отправленных.


    public class ReporterTests
    {
        //Создаем тест, который описывает 4-ое требование:

        /*     [Fact]
             public void ReturnNumberOfSentReports()
             {
                 var reporter = new Reporter.Reporter();

                 var reportCount = reporter.SendReports();

                 Assert.Equal(2, reportCount);
             }*/

        //Возвращаемся к проектированию.Я думаю, что у меня будет отдельный класс для создания отчётов,
        //и класс для отправки отчётов.Сам объект Reporter будет управлять логикой взаимодействия
        //этих классов. Назовем первый объект IReportBuilder,
        //а второй – IReportSender.Попроектировали, пора написать код:

    /*    [Fact]
        public void ReturnNumberOfSentReports()
        {
            IReportBuilder reportBuilder ; // автор чего то покурил, грязное животное
            IReportSender reportSender ;

            //var reporter = new Reporter.Reporter(reportBuilder, reportSender);

            //var reportCount = reporter.SendReports();

           // Assert.Equal(2, reportCount);
        }*/

       // Вопрос: Как задать поведение объектов, с которыми взаимодействует наш тестируемый класс?
        //Вместо реальных объектов, с которыми взаимодействует наш тестируемый класс удобнее всего
        //использовать заглушки или mock-объекты.В текущем приложении мы будем создавать mock-объекты
        //с помощью библиотеки Moq.


        [Fact]
        public void ReturnNumberOfSentReports_WithMockFraud()
        {
            var reportBuilderExpected = new Mock<IReportBuilder>();
            var reportSender = new Mock<IReportSender>();
            // задаем поведение для интерфейса IReportBuilder
            // Здесь говорится: "При вызове функции CreateReports вернуть List<Report> состоящий из 2х объектов"
            reportBuilderExpected.Setup(m => m.CreateRegularReports())
                    .Returns(new List<Report> { new Report(), new Report() });

            var reporter = new Reporter.Reporter(reportBuilderExpected.Object, reportSender.Object);
            var reportCountActual = reporter.SendReports();

            Assert.Equal(2, reportCountActual);
        }

        //Теперь займёмся первым требованием – отправлением отчётов.Тест будет проверять, что все созданные отчёты отправлены:

        [Fact]
        public void SendAllReports()
        {
            // arrange
            var reportBuilder = new Mock<IReportBuilder>();
            var reportSender = new Mock<IReportSender>();

            reportBuilder.Setup(m => m.CreateRegularReports())
              .Returns(new List<Report> { new Report(), new Report() });

            var reporter = new Reporter.Reporter(reportBuilder.Object, reportSender.Object);

            // act
            reporter.SendReports();

            // assert
           reportSender.Verify(m=> m.Send(It.IsAny<Report>()),Times.Exactly(2));
        }

        //Пора подумать о том, как реализовывать третье требование.
        //С чего начнем? Нарисуем UML-диаграммы или просто помедитируем сидя в кресле? Начнём с теста!
        //Запишем 3-е бизнес-требование в коде:
        [Fact]
        public void SendSpecialReportToAdministratorIfNoReportsCreated()
        {
            var reportBuilder = new Mock<IReportBuilder>();
            var reportSender = new Mock<IReportSender>();
            //3) Если ни одного отчета не сформировано, то отправляем сообщение руководству о том,
            reportBuilder.Setup(m => m.CreateRegularReports()).Returns(new List<Report>());
            //формируетм специальный отчет, что приколько среда сама может генерировать нужные методы  и классы через лампочку, основываяс на тесте
            reportBuilder.Setup(m => m.CreateSpecialReport()).Returns(new SpecialReport());

            var reporter = new Reporter.Reporter(reportBuilder.Object, reportSender.Object);
            reporter.SendReports();

            //Assert
            reportSender.Verify(m=> m.Send(It.IsAny<Report>()),Times.Never());
            //так же студия подсказывает создать метод Send SpecialReport
            reportSender.Verify(m => m.Send(It.IsAny<SpecialReport>()), Times.Once);
            //тест провален, ведь мы еше не реализовали поведение идем кодить
            //добавили логику
            //
        }

        //2) Каждый второй сформированный отчет надо отправлять ещё и аудиторам.
        [Fact]
        public void SendEverySecondReportForAuditors()
        {
            var reportBuilder = new Mock<IReportBuilder>();
            var reportSender = new Mock<IReportSender>();
            //2) Каждый второй сформированный отчет надо отправлять ещё и аудиторам.
            reportBuilder.Setup(m => m.CreateRegularReports()).Returns(new List<Report> { new Report(), new Report(), new Report(), new Report(), new Report() });
            reportBuilder.Setup(m => m.CreateAuditorReport()).Returns(new AuditorReport());    

            var reporter = new Reporter.Reporter(reportBuilder.Object, reportSender.Object);
            reporter.SendReports();

            //Assert
            reportSender.Verify(m => m.Send(It.IsAny<Report>()));
            //так же студия подсказывает создать метод Send SpecialReport
            reportSender.Verify(m => m.Send(It.IsAny<AuditorReport>()), Times.AtLeast(2));
            //тест провален, ведь мы еше не реализовали поведение идем кодить
            //добавили логику
            //
        }
    }
}
