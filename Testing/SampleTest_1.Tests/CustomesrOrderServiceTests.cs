﻿using NUnit.Framework;
using CustomerOrderService;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerOrderService.Tests
{
    [TestFixture]
    public class CustomesrOrderServiceTests
    {
        [TestCase]
        public void When_PremiumCustomer_Expect_10PercentDiscoun()
        {
            //Arrange
            Customer premiumCustomer = new Customer
            { 
                CustomerId = 1,
                CustomerName = "TestFoo",
                CustomerType = CustomerType.Premium
            };

            Order order = new Order
            {
                OrderId = 1,
                ProductId = 212,
                ProductQuantity = 1,
                Amount = 999
            };

            CustomesrOrderService customesrOrderService = new CustomesrOrderService();

            //Act
            customesrOrderService.ApplyDiscount(premiumCustomer, order);
            var actual =(double) order.Amount;
            //Assert

            Assert.AreEqual( 899.1d, actual, 0.1d); //пеоегрузка с дельтой


        }
    }
}