﻿using NUnit.Framework;
using SampleTest_1.Prime;
using System;
using System.Collections.Generic;
using System.Text;

namespace SampleTest_1.Prime.Tests
{
    [TestFixture]
    public class PrimeService_IsPrimeShould
    {
        private PrimeService _ps;

        [SetUp]
        public void SetUp() 
        {
            _ps = new PrimeService();
        }


        [Test]
        public void IsPrimeTest_ReturnFalse()
        {
            var result = _ps.IsPrime(1);
            Assert.IsFalse(result, "1 not prme");
        }

        [TestCase(-1)]
        [TestCase(0)]
        [TestCase(1)]
        public void IsPrime_ValuesLessThan2_ReturnFalse(int value)
        {
            var result = _ps.IsPrime(value);

            Assert.IsFalse(result, $"{value} should not be prime");
        }
    }
}