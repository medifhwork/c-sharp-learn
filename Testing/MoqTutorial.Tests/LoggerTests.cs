﻿using NUnit.Framework;
using MoqTutorial;
using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using System.Linq;

namespace MoqTutorial.Tests
{
    [TestFixture]
    public class LoggerTests
    {
        // 1.Проверка вызова метода ILogWriter.Write объектом класса Logger(с любым аргументом):
        [Test]
        public void Foo1()
        {
            var mock = new Mock<ILogWriter>();
            var logger = new Logger(mock.Object);

            logger.WriteLine("Hello, logger!");

            // Проверяем, что вызвался метод Write нашего мока с любым аргументом
            mock.Verify(lw => lw.Write(It.IsAny<string>()));
        }


        //2.Проверка вызова метода ILogWriter.Write с заданным аргументами:
        [Test]
        public void Foo2()
        {
            var mock = new Mock<ILogWriter>();
            var logger = new Logger(mock.Object);

            logger.WriteLine("Hello, logger!");

            mock.Verify(lw => lw.Write("Hello, logger!"));
        }

        //3. Проверка того, что метод ILogWriter.Write вызвался в точности один раз (ни больше, ни меньше):
        [Test]
        public void Foo3()
        {
            var mock = new Mock<ILogWriter>();
            var logger = new Logger(mock.Object);

            logger.WriteLine("Hello, logger!");

            mock.Verify(lw => lw.Write(It.IsAny<string>()),
                                               Times.Once());
        }

        //4.Проверка поведения с помощью метода Verify (может быть удобной, когда нужно проверить несколько допущений):
        /*В некоторых случаях неудобно использовать несколько методов Verify для проверки нескольких вызовов.
         * Вместо этого можно создать мок-объект и задать ожидаемое поведение с помощью методов Setup и 
         * проверять все эти допущения путем вызова одного метода Verify(). Такая техника может быть удобной 
         * для повторного использования мок-объектов, создаваемых в методе Setup теста.*/
        [Test]
        public void Foo4()
        {
            var mock = new Mock<ILogWriter>();
            mock.Setup(lw => lw.Write(It.IsAny<string>()));
            mock.Setup(lw => lw.SetLogger(It.IsAny<string>()));
            var logger = new Logger(mock.Object);

            logger.WriteLine("Hello, logger!");

            // Мы не передаем методу Verify никаких дополнительных параметров.
            // Это значит, что будут использоваться ожидания установленные
            // с помощью mock.Setup
            mock.Verify();
        }

        /*Moq поддерживает две модели проверки поведения: строгую (strict) и свободную (loose).
         * По умолчанию используется свободная модель проверок, которая заключается в том,
         * что тестируемый класс (Class Under Test, CUT), во время выполнения действия 
         * (в секции Act) может вызывать какие угодно методы наших зависимостей и 
         * мы не обязаны указывать их все.
         *
         * Так, в предыдущем примере метод logger.WriteLine вызывает два метода интерфейса
         * ILogWriter: метод Write и SetLogger. При использовании MockBehavior.
         * Strict метод Verify завершится неудачно, если мы не укажем явно, 
         * какие точно методы зависимости будут вызваны:*/
        [Test]
        public void Foo5()
        {
            var mock = new Mock<ILogWriter>(MockBehavior.Strict);
            // Если закомментировать одну из следующих строк, то
            // метод mock.Verify() завершится с исключением
            mock.Setup(lw => lw.Write(It.IsAny<string>()));
            mock.Setup(lw => lw.SetLogger(It.IsAny<string>()));

            var logger = new Logger(mock.Object);
            logger.WriteLine("Hello, logger!");

            mock.Verify();
        }

        /*Класс MockRepository предоставляет еще один синтаксис для создания стабов и,
         * что самое главное, позволяет хранить несколько мок-объектов и проверять более 
         * комплексное поведение путем вызова одного метода.

        1. Использование MockRepository.Of для создания стабов.
        Данный синтаксис аналогичен использованию Mock.Of, однако позволяет задавать поведение 
        разных методов не через оператор &&, а путем использования нескольких методов Where:*/

        [Test]
        public void Foo6()
        {
            var repository = new MockRepository(MockBehavior.Default);
            ILoggerDependency logger = repository.Of<ILoggerDependency>()
                .Where(ld => ld.DefaultLogger == "DefaultLogger")
                .Where(ld => ld.GetCurrentDirectory() == "D:\\Temp")
                .Where(ld => ld.GetDirectoryByLoggerName(It.IsAny<string>()) == "C:\\Temp")
                .First();

            Assert.That(logger.GetCurrentDirectory(), Is.EqualTo("D:\\Temp"));
            Assert.That(logger.DefaultLogger, Is.EqualTo("DefaultLogger"));
            Assert.That(logger.GetDirectoryByLoggerName("CustomLogger"), Is.EqualTo("C:\\Temp"));
        }

        /*2. Использование MockRepository для задания поведения нескольких мок-объектов.
             Предположим, у нас есть более сложный класс SmartLogger, которому требуется две зависимости:
             ILogWriter и ILogMailer.Наш тестируемый класс при вызове его метода Write
             должен вызвать методы двух зависимостей:*/
        [Test]
        public void Foo7()
        {
            var repo = new MockRepository(MockBehavior.Strict);
            var logWriterMock = repo.Create<ILogWriter>();
            logWriterMock.Setup(lw => lw.Write(It.IsAny<string>()));

            var logMailerMock = repo.Create<ILogMailer>();
            logMailerMock.Setup(lm => lm.Send(It.IsAny<MailMessage>()));

            var smartLogger = new SmartLogger(logWriterMock.Object, logMailerMock.Object);

            smartLogger.WriteLine("Hello, Logger");
            smartLogger.SendMail(new MailMessage());

            repo.Verify();
        }

        /*Другие техники*/

        /*В некоторых случаях бывает полезным получить сам мок-объект
         * по интерфейсу(получить Mock<ISomething> по интерфейсу ISomething).
         * Например, функциональный синтаксис инициализации заглушек возвращает 
         * не мок-объект, а сразу требуемый интерфейс.Это бывает удобным для 
         * тестирования пары простых методов, но неудобным, если понадобится 
         * еще и проверить поведение, или задать метод, возвращающий разные 
         * результаты для разных параметров. Так что иногда бывает удобно использовать 
         * LINQ-based синтаксис для одной части методов и использовать методы Setup – для другой:*/
        [Test]
        public void Foo8()
        {
            ILoggerDependency logger = Mock.Of<ILoggerDependency>(
                 ld => ld.GetCurrentDirectory() == "D:\\Temp"
                       && ld.DefaultLogger == "DefaultLogger");

            // Задаем более сложное поведение метода GetDirectoryByLoggerName
            // для возвращения разных результатов, в зависимости от аргумента
            Mock.Get(logger)
                .Setup(ld => ld.GetDirectoryByLoggerName(It.IsAny<string>()))
                .Returns<string>(loggerName => "C:\\" + loggerName);

            Assert.That(logger.GetCurrentDirectory(), Is.EqualTo("D:\\Temp"));
            Assert.That(logger.DefaultLogger, Is.EqualTo("DefaultLogger"));
            Assert.That(logger.GetDirectoryByLoggerName("Foo"), Is.EqualTo("C:\\Foo"));
            Assert.That(logger.GetDirectoryByLoggerName("Boo"), Is.EqualTo("C:\\Boo"));
        }


    }

}