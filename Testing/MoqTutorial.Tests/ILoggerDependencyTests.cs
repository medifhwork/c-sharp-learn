using NUnit.Framework;
using MoqTutorial;
using Moq;

namespace MoqTutorial.Tests
{
    public class ILoggerDependencyTests
    {
        //���� ������ GetCurrentDirectory:
        [Test]
        public void Foo1()
        {
            // Mock.Of ���������� ���� ����������� (������-������), � �� ���-������.
            // ��������� ��� ��������, ��� ��� ������ GetCurrentDirectory()
            // �� ������� "D:\\Temp"
            ILoggerDependency loggerDependency =
                Mock.Of<ILoggerDependency>(d => d.GetCurrentDirectory() == "D:\\Temp");
            //�� �������� stub.,  ��� ��������(� �������� ������� ���)
            var currentDirectory = loggerDependency.GetCurrentDirectory();

            Assert.That(currentDirectory, Is.EqualTo("D:\\Temp"));
        }

        // ���� ������ GetDirectoryByLoggerName, ������ ������������ ���� � ��� �� ���������:
        [Test]
        public void Foo2()
        {
            // ��� ������ ��������� ������ GetDirectoryByLoggerName ������� "C:\\Foo".
              ILoggerDependency loggerDependency = Mock.Of<ILoggerDependency>(
                ld => ld.GetDirectoryByLoggerName(It.IsAny<string>()) ==  "C:\\Foo" );

       

            string directory = loggerDependency.GetDirectoryByLoggerName("anything");

            System.Console.WriteLine("GetDirectoryByLoggerName returned " + directory);

            Assert.That(directory, Is.EqualTo("C:\\Foo"));
        }

        //���� ������ GetDirrectoryByLoggerName, ������������ ��������� � ����������� �� ���������:
        [Test]
        public void Foo3()
        {
            // �������������� �������� ����� �������, ����� ������������ ��������
            // ������ GetDirrectoryByLoggerName �������� �� ��������� ������.
            // ��� ���������� �������� ����:
            // public string GetDirectoryByLoggername(string s) { return "C:\\" + s; }
            Mock<ILoggerDependency> stub = new Mock<ILoggerDependency>();
            stub.Setup(ld => ld.GetDirectoryByLoggerName(It.IsAny<string>()))
                .Returns<string>(name => "C:\\" + name);//.Callback((string x) => System.Console.WriteLine(x)); // "SomeLogger"

            string loggerName = "SomeLogger";
            ILoggerDependency logger = stub.Object;
            string directory = logger.GetDirectoryByLoggerName(loggerName);

            Assert.That(directory, Is.EqualTo("C:\\" + loggerName));
        }

        //���� �������� DefaultLogger:
        [Test]
        public void Foo4()
        {// �������� DefaultLogger ����� �������� ����� ���������� ��������� ��������
            ILoggerDependency logger = Mock.Of<ILoggerDependency>(
                d => d.DefaultLogger == "DefaultLogger");

            string defaultLogger = logger.DefaultLogger;

            Assert.That(defaultLogger, Is.EqualTo("DefaultLogger"));
        }

        //������� ��������� ���������� ������� ����� ���������� � ������� �moq functional specification� (�������� � Moq v4):
        [Test]
        public void Foo5()
        {
            // ���������� �������� ������ ������� � ������� ����������� �Ȼ
            ILoggerDependency logger =
                  Mock.Of<ILoggerDependency>(
                      d => d.GetCurrentDirectory() == "D:\\Temp" &&
                           d.DefaultLogger == "DefaultLogger" &&
                           d.GetDirectoryByLoggerName(It.IsAny<string>()) == "C:\\Temp");

            Assert.That(logger.GetCurrentDirectory(), Is.EqualTo("D:\\Temp"));
            Assert.That(logger.DefaultLogger, Is.EqualTo("DefaultLogger"));
            Assert.That(logger.GetDirectoryByLoggerName("CustomLogger"), Is.EqualTo("C:\\Temp"));
        }

        //������� ��������� ���������� ������� � ������� ������ ������� Setup (������� v3 ���������):
        [Test]
        public void Foo6()
        {
            var stub = new Mock<ILoggerDependency>();
            stub.Setup(ld => ld.GetCurrentDirectory()).Returns("D:\\Temp");
            stub.Setup(ld => ld.GetDirectoryByLoggerName(It.IsAny<string>())).Returns("C:\\Temp");
            stub.SetupGet(ld => ld.DefaultLogger).Returns("DefaultLogger");

            ILoggerDependency logger = stub.Object;

            Assert.That(logger.GetCurrentDirectory(), Is.EqualTo("D:\\Temp"));
            Assert.That(logger.DefaultLogger, Is.EqualTo("DefaultLogger"));
            Assert.That(logger.GetDirectoryByLoggerName("CustomLogger"), Is.EqualTo("C:\\Temp"));
        }
        /*������������� �� �������� ������� Setup ����� ����, ��-������, ����� ������������, 
         * � ��-������, ��� �� ������������� �� ������ �������, 
         * ���������� �� �� ��������� ��������� ��� ���������.*/


    }
}