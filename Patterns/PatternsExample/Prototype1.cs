﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    [Serializable] //можно для не сложных объектов без зависимостей.
    public class Contact
    {
        public string Name;
        public Address WorkAddress;

        public override string ToString()
        {
            return $"Name: {Name}, WorkAddress: {WorkAddress}";
        }
    }

    [Serializable]
    public class Address
    {
        public string Street;
        public string city;
        public int Suite;

        public override string ToString()
        {
            return $"Street: {Street}, City: {city}, Suite: {Suite}";
        }
    }

    /// <summary>
    /// универсальный метод проводящий полную гопию объекта на осонове сериализации
    /// подойдет для не сложных объектов с полностью известным исходным кодом.
    /// </summary>
    static class ExtentionMethods
    {
        public static T TryCreateDeepCopy<T>(this T self)
        {
            if (!typeof(T).IsSerializable)
                throw new ArgumentException("Type must be serializeble");
            if (ReferenceEquals(self, null))
                return default(T);

            var formatter = new BinaryFormatter();
            using (var stream = new MemoryStream())
            {
                formatter.Serialize(stream, self);
                stream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(stream);
            }
        }
    }

    //Реализация с общим хранилищем прототипов на базе паттерна фабричного метода
    public class EmployeeFactory
    {
        private static Contact main = new Contact
        {
            WorkAddress = new Address
            { city = "NewYork", Street = "Manhatten 123" }
        };

        private static Contact aux = new Contact
        {
            WorkAddress = new Address
            { city = "NewYork", Street = "Manhatten 125" }
        };

        private static Contact NewEmployee(string name,int suite, Contact prototype)
        {
            var result = prototype.TryCreateDeepCopy();
            result.Name = name;
            result.WorkAddress.Suite = suite;
            return result;
        }

        //прототип
        public static Contact NewMainOfficeEmployee(string name,int suite)
        {
            return NewEmployee(name, suite, main);
        }

        public static Contact NewAuxOfficeEmployee(string name, int suite)
        {
            return NewEmployee(name, suite, aux);
        }
    }


    static class RunPrototypeDemo
    {
       public static void Run()
        {
            var john = EmployeeFactory.NewMainOfficeEmployee("John", 120);
            var jillValentine = EmployeeFactory.NewAuxOfficeEmployee("jillValentine", 128);
            Console.WriteLine($"{john}\n{jillValentine}");

        }
    }
}
