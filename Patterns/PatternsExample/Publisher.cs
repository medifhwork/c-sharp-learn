﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
 {      /// <summary>
        /// Observer  - наблюдатель
        /// Паттерн Observer – использует связь отношения зависимости «один ко многим»
        /// (один издатель ко многим подписчикам). При изменении состояния одного объекта (издателя),
        /// все зависящие от него объекты (подписчики) оповещаются об этом и автоматически обновляются.
        /// </summary>
    public class Publisher
    {
       public void Foo()
        {
            Console.WriteLine("Do work...somthing");
            Console.ReadKey();
        }
        public void SendMessTo(Subscriber subscriber)
        {
            subscriber.CallFooOn(this);
        }

    }
    public class Subscriber
    {
       public void CallFooOn(Publisher publisher) 
       {
            publisher.Foo();
       }
    }

    public static class RunDemoPublisher
    {
        public static void Run()
        {
            Publisher publisher1 = new Publisher();
            Subscriber subscriber1 = new Subscriber();
            //посылка сообщения 
            publisher1.SendMessTo(subscriber1);
        }
    }
}
