﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    interface IAnimal { }

    interface IActions
    {
        void DoAction();
    }

    interface Iwalker
    {
        void Walk();
    }

    interface ISwimmer
    {
        void Swim();
    }
       
    class WalkAction : IActions
    {
        public virtual  void DoAction()
        {
            // Console.WriteLine("walk");
            Console.WriteLine("fast walk");
        }
    }

    class SwimAction : IActions
    {
        public void DoAction()
        {
            Console.WriteLine("swim");
        }
    }

    class SwimButerflyAction : WalkAction
    {
        public override void  DoAction()
        {
            Console.WriteLine("swim butterfly");
        }
    }

    //how using
    class Penguin : IAnimal, Iwalker, ISwimmer
    {// Можем менять набор интерфейсов
        IActions _walkAction;
        IActions _swimAction;

        public Penguin()
        {//первая точка гибкости, можно подменить
         //объектом c иным поведением, IActions
            _walkAction = new WalkAction();
            _swimAction = new SwimAction();
            _swimAction = new SwimButerflyAction();
        }

        public void Swim()
        {
            //_swimAction.DoAction();
            // new SwimAction().DoAction();
            CustomAction(new SwimAction());
        }

        public void Walk()
        {
            _walkAction.DoAction();
        }

        public void CustomAction (IActions actions) 
        {
            actions.DoAction();
        }

    }

    public static class RunDemoStrategy
    {
        public static void Run()
        {
            Console.WriteLine("This pattern the Stategy");
            var penguin = new Penguin();
            penguin.Walk();
            penguin.Swim();
            Console.WriteLine();
            Console.ReadKey();
        }
    }

}



/*
 https://www.youtube.com/watch?v=-AaVsHkgWcQ&t=10s 
 
 Стратегия — это поведенческий паттерн проектирования,
который определяет семейство схожих алгоритмов и помещает 
каждый из них в собственный класс, после чего алгоритмы можно 
взаимозаменять прямо во время исполнения программы.


 
 
 */