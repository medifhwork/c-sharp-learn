﻿using System;

namespace Patterns
{
    class Program
    {
        static void Main(string[] args)
        {
            //RunDemoStrategy.Run();
            //RunDemoPublisher.Run();
            //RunDemoSingletonNotSafe.Run();
            //RunDemoSingletonSafeThreading.Run();
            //RunPrototypeDemo.Run();
            //RunPrototype2Demo.Run();
            //RunDecoratorDemo.Run();
            //RunTemplateMethod.Run();
            //RunStrategy2Demo.Run();
            //AbstractFactoryDemo.Run();
            ProxyFactoryDemo.Run();
        }
    }
}
