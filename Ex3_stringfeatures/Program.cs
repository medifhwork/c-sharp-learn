﻿using System;

namespace Ex3_stringfeatures
{
    internal class Program
    {
        static void Main(string[] args)
        {          

            string FIO1 = "Aasd Asdsd Ddfdfdfdfdf";
            string FIO2 = "Aаа    Ббб     Ввввв";

            ///string[]  splitedFIO  = FIO2.Split("\\s{2,}");
            string @IO = "";
            var splitedFIO = System.Text.RegularExpressions.Regex.Split(FIO2, @"\s{1,}");
            if (splitedFIO.Length == 3)
            {
               @IO = splitedFIO[0] + "-" + splitedFIO[1] + "-" + splitedFIO[2];
            }
            else @IO = "non";
            Console.WriteLine(@IO);

        }
    }
}
