﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson11_LinqLazy
{
    class Program
    {
        static void Main(string[] args)
        {/*
          Результатом работы больлинства LINQ инструкций (кроме агрегирующих)
            является механизм получения результата, а не сам результат.
            При изменении исходных данных результат выполнения также изменится

            инымы словаими полная анология с классическими запросами, пока не вызвал на выполнение 
            данных нет.
          */

            int[] arr ={-5, -3, 4,0,3,6,-33 };
            var res = arr.Where(n => n > 0);
            Console.WriteLine(res.Count()); //3
            arr[0] = 1;
            Console.WriteLine(res.Count()); //4
            Console.WriteLine("---------------------");

            var rndArr = GetRandomCollection(4);//здесть вычисления не проихсодят
            rndArr = GetRandomCollection(4).ToArray();  //принудительно вычисляет результат и магия пропадет
            Print(rndArr);//тут получаем перечислитель и  рандом заполняет
            Console.WriteLine("- - - - - - - ");
            Print(rndArr);//тут получаем перечислитель и снова рандом заполняет

        }

        /*Все отложенные вычисления основаны на yeld return(IEnumerable)*/
        /*Как это работает под копотом в IL код*/ 
        static IEnumerable<int> GetRandomCollectionOldStyle(int count, int minValue = 0, int maxValue = 100)
        {//если положить в переменную резульат, при выводе всегда будет одно и то же
            var rnd = new Random();
            int[] result = new int[count];
            for (int i = 0; i < count; i++)
            {
                result[i] = rnd.Next(minValue, maxValue);
                System.Threading.Thread.Sleep(200);
            }
            return result;
        }
        /*то жесамое но с использывание специального оператора генерации программных текстов*/
        static IEnumerable<int> GetRandomCollection(int count, int minValue = 0, int maxValue = 100)
        {//если положить в переменную резульат, при выводе N раз будет N разных массивов, с поправкой на работу Random
            var rnd = new Random();
            for (int i = 0; i < count; i++)
            {// "складывает в себе" все резульататы, и только достигнув последнего отрабатывает return.
                yield return rnd.Next(minValue, maxValue);
                System.Threading.Thread.Sleep(20);
            }
        }

       static void Print (IEnumerable<int> collection)
        {
            foreach (var item in collection)
            {
                Console.WriteLine(item);
            }
        }

    }
}
