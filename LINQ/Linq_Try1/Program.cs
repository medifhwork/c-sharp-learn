﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq_Try1
{
    class Program
    {
        public delegate string Rnd();

        static void Main(string[] args)
        {
            string[] teams = { "Бавария", "Боруссия", "Реал Мадрид", "Манчестер Сити", "ПСЖ", "Барселона" };

            var selectedTeams = from t in teams // определяем каждый объект из teams как t  (хм похоже на foreach)
                                where t.ToUpper().StartsWith("Б") //фильтрация по критерию
                                orderby t  // упорядочиваем по возрастанию
                                select t; // выбираем объект

            var selectedTeams2 = teams.Where(x => x.ToUpper().StartsWith("Б")).OrderBy(x => x);

            foreach (string s in selectedTeams)
                Console.WriteLine(s);

            Console.WriteLine("---------");

            foreach (string s in selectedTeams2)
                Console.WriteLine(s);


            String[] c = { "green", "brown" };
            Console.WriteLine(c.Where(d => d.Length >4).OrderBy(x => x).SingleOrDefault(
                (x) =>
                {
                    Console.WriteLine(x);
                    return false;
                }
                ));
        }
             
    }
}


