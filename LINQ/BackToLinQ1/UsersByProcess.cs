﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackToLinQ1
{
    internal  class UsersByProcess
    {
        public string ProcessName;
        public string TaskNumber;
        public User User;
        public string Role;
        public int StayPoint;
    }

    internal class User:IComparer<User>, IComparable
    {
        public int Id;
        public string FullName;

        public int Compare(User x, User y)
        {
            
            return x.Id - y.Id;
           
        }

        public int CompareTo(object obj)
        {
            //int res = 0;
            //if (obj is User)
            //    res = this.Id - ((User)obj).Id;
            //else res = -1;
            //return res;
            return ((User)obj).Id - this.Id;
        }

        
    public override bool Equals(object obj)
    {
        return ((User)obj).Id == this.Id;
    }
    public override int GetHashCode()
    {
        return this.Id.GetHashCode();
    }
    }
}
