﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackToLinQ1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var users = Factory.UsersByProcess;
            foreach (var user in users) Console.WriteLine($"{user.ProcessName} {user.TaskNumber} | {user.User.FullName:100} | {user.Role:-50} | staypoint {user.StayPoint}");   
            Console.WriteLine();
            var foo = users
                .Where(c => c.ProcessName != "Portfolio 2")
                .Where(c => c.TaskNumber != "3B");                
            foreach (var user in foo) Console.WriteLine($"{user.ProcessName} {user.TaskNumber} | {user.User.FullName:100} | {user.Role:-50} | staypoint {user.StayPoint}");
            Console.WriteLine();
            //var bar = 
            foo
                .Select(s => new { TaskNumber = s.TaskNumber, User = s.User, Role = s.Role });
            ///var zak = bar.Distinct();
            foreach (var user in foo) Console.WriteLine($"{user.TaskNumber} | {user.User.FullName} | {user.Role:-50}");
            Console.WriteLine( );
            foo.Select(s => new { TaskNumber = s.TaskNumber, User = s.User, Role = s.Role })
                .ToList().Distinct();
            ///var zak = bar.Distinct();
            foreach (var user in foo) Console.WriteLine($"{user.TaskNumber} | {user.User.FullName} | {user.Role:-50}");

            Console.ReadKey();
        }
    }
}
