﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackToLinQ1
{
    public static class Factory
    {
        static Factory()
        {
            usersByProcess = new UsersByProcess[]
            {   new UsersByProcess () {ProcessName = "Portfolio 1",
                                       TaskNumber ="1A", 
                                       User = new User() {Id = 1, FullName = "Давыдов Петр Михайлович" },
                                       Role = "Инициатор стороны 1",
                                       StayPoint = 1},
                new UsersByProcess () {ProcessName = "Portfolio 1",
                                       TaskNumber ="1A",
                                       User = new User() {Id = 1, FullName = "Давыдов Петр Михайлович" },
                                       Role = "Инициатор стороны 1",
                                       StayPoint = 2},
                new UsersByProcess () {ProcessName = "Portfolio 1",
                                       TaskNumber ="2A",
                                       User = new User() {Id = 2, FullName = "Коробко Зина Власовна" },
                                       Role = "Руководитель стороны 1"
                                        , StayPoint = 1},
                new UsersByProcess () {ProcessName = "Portfolio 1",
                                       TaskNumber ="3B",
                                       User = new User() {Id = 3, FullName = "Подкоблучников Кирил Сергеевич" },
                                       Role = "Руководитель стороны 2", StayPoint = 1},
                new UsersByProcess () {ProcessName = "Portfolio 2",
                                       TaskNumber ="1A",
                                       User = new User() {Id = 1, FullName = "Давыдов Петр Михайлович" },
                                       Role = "Инициатор стороны 1", StayPoint = 1}
            };
        }

        private static UsersByProcess[] usersByProcess;

        internal static UsersByProcess[] UsersByProcess { get => usersByProcess; set => usersByProcess = value; }
    }
}
