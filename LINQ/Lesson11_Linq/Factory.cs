﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson11_Linq
{
    public static class Factory
    {
        static Factory()
        {

            var Developers = new Department { id = 1, NuberOfPeople = 3, Name = "Developers"};
            var HRdep = new Department { id = 2, NuberOfPeople = 2, Name = "HR"};
            var Sales = new Department { id = 3, NuberOfPeople = 2, Name = "Sale" };
            var Managers = new Department { id = 4, NuberOfPeople = 2, Name = "Managers" };
            Departments = new Department[] { Developers , HRdep , Sales, Managers };

            HR = new Employee() { Name = "<Мaша>", Age = 19, Payment = 0_500, StartTime = DateTime.Parse("23.01.2021"), FK_Id_Department = 2};

            _employees = new Employee[]
            {
                HR,
                new Employee() {Name="Антон", Age=25,  Payment=10_000, StartTime=DateTime.Parse("23.02.2015"), FK_Id_Department = 4 },
                new Employee() {Name="Джон",  Age=32,  Payment=15_000, StartTime=DateTime.Parse("23.04.2010"), FK_Id_Department = 1 },
                new Employee() {Name="Нина",  Age=20,  Payment=8_000,  StartTime=DateTime.Parse("20.01.2019"), FK_Id_Department = 4 },
                new Employee() {Name="Некo",  Age=120, Payment=50_000, StartTime=DateTime.Parse("28.06.1990"), FK_Id_Department = 1 },
                new AdvanceEmployee() {Name="Куплинов", Age=34, Payment=100_000, StartTime=DateTime.Parse("01.01.1986"), SuperSkill="swears", FK_Id_Department = 2}
            };

            _employees2 = new Employee[]
            {
                HR,
                new Employee() {Name="Мария", Age=25,Payment=10_000, StartTime=DateTime.Parse("23.02.2015")  , FK_Id_Department =3},
                new Employee() {Name="Шерлок", Age=32,Payment=15_000, StartTime=DateTime.Parse("23.04.2010") , FK_Id_Department =3}
            };
        }


        private static Employee[] _employees;

        public static Employee[] GetEmployees() => _employees;

        private static Employee[] _employees2;

        public static Employee[] GetEmployees2() => _employees2;

        private static Employee HR { get; set; }

        public static Department[] Departments { get => _departments; private set => _departments = value; }

        private static Department[] _departments;

        public static Department[] GetDepartments() => _departments;

    }
}
