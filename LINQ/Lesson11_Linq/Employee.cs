﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson11_Linq
{

    public class Human { }

    public class Employee : Human
    {
        public int Age { get; set; }
        public string Name { get; set; }
        public int Payment { get; set; }
        public DateTime StartTime { get; set; }
        public int FK_Id_Department { get; set; }
    }


    public class AdvanceEmployee : Employee
    {
        public string SuperSkill { get; set; }
    }
}
