﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson11_Linq
{
    class Flower
    {
        public string Name { get; set; }
        public int Length { get; set; }
    }
}
