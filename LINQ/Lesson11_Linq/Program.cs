﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson11_Linq
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee[] e = Factory.GetEmployees();
            Department[] d = Factory.GetDepartments();

            //Цепочка вызовов, текучий код, основан на методах расширения класса IEnumerable
            var numbers33 = new[] { 1, 4, 6, 3, 5, 2, 7, 9, 8 };
            var result0 = numbers33.Where(x => x % 2 == 0) // сначала отберет четные и передаст в Select
                   .Select(x => x * x) //изменит представлкние  4->16, 6->36 итд
                   .Skip(1)  // пропустит первый элемент 36,4,64
                   .Take(2) // взять только первые 2 - 36,4
                   .Min();  // выбрать минимальный из них 4
            

            //Маппирование или проицирование .Select

            // получим строки обернутые в коллекцию 
            // иными словами select выводит одну строку за раз сформированную из объектов в колекции
            var result = e.Select(x => x.Name);
            var result1 = e.Where(x => x.Age > 20).Select(x => x.Name);

            // получим объекты Employee обернутые в коллекцию IEnumerable<Employee>
            var result3 = e.Where(x => x.Age > 20);

            // развернутый синтаксис + краткий 
            var result4 = e.Where(x =>
            {
                return x.Age < 30;
            }
                                 ).Where(x => x.Payment <= 15_000);

            // комбинация условия в одном уровне текущего синтаксиса
            var result4_1 = e.Where(x =>
            {
                return x.Age < 30 && x.Payment <= 15_000;
            });

            // Проецирование нескольких полей, через анонимный тип.
            var result5 = e.Where(x => x.StartTime > DateTime.Parse("01.01.2015"))
                .Select(x => new
                {
                    x.Name,   //отобрать несколько полей через 
                    x.Age,    //анонимный тип по слабой ссылке
                              //см h003_AnonimusTypeAndIntroduceLinq
                    x.StartTime
                });

            // если же нужно прокинуть куда то дальше
            // пилим новый тип с нужными полями и инстанцируем его
            var result5_1 = e.Where(x => x.StartTime > DateTime.Parse("01.01.2015"))
               .Select(x => new Employee
               {
                   Name = x.Name,
                   Age = x.Age,
                   Payment = x.Payment,
                   StartTime = x.StartTime
               });

            //Сортировка коллекции по ключю
            var result6 = e.OrderByDescending(x => x.Age);

            //Венрнуть первый элемент коллекции, может быть исключение если колекция пуста
            var result15 = e.First();

            //Вернуть первый элемент коллекции или default типа
            var result16 = e.FirstOrDefault();

            //Количество элементов коллекции, вниманее проитерирует всю коллекцию, если у вашего типа есть Length поле предпочтительнее
            var result17 = e.Count();

            //Any возвращает bool, соответвенно полезнее в комбинации с were или join
            //чекает всю коллекцию и если хотя бы 1 элемент удовлетворяет условию то true или колекция пустая тоже true
            var result18 = e.Any(abracadabra => abracadabra.Age > 100);

            //отобрали работников с зп более 15к и узнали есть ли среди них кто старше 100 лет, типа да
            var result19 = e.Where(x => x.Payment > 45000)
                              .Any(abracadabra => abracadabra.Age > 100);

            //SQL All вернет тру если все члены выборки удовлетворяют условию
            var result19_1 = e.Where(x => x.Payment > 45000)
                              .All(abracadabra => abracadabra.Age > 100);

            // будет True потому что так работает All если ссылка пустая.
            string[] numbers12 = { };
            var result122 = numbers12.All(n => n.Length == 0);

            // Any без параметров вернет НЕ пустали ли колекция
            var result20 = e.Any();

            // вернет инт
            var result21 = e.Max(x => x.Age);

            //Что бы вернуть обьект типа имеющий MAX значене по ключю
            var result22 = e.FirstOrDefault(x => x.Age == e.Max(y => y.Age));

            //c упаковкой в коллекцию по результату одно и тоже с предыдущим, но через сортировку, что повлияет на производительность
            IEnumerable<Employee> result23 = new Employee[] { e.OrderByDescending(x => x.Age).FirstOrDefault() };


            // выводит среднеий возраст работника
            var result24 = e.Average(x => x.Age);


            /*                       каст всех элементов колекции к типу                */


            // в случае с Human  - upcast - Employee->Human
            var result25 = e.Cast<Human>();

            // в случае с AdvanceEmployee  Invalid Cast Ex
            var result26 = e.Cast<AdvanceEmployee>();

            // выборка из коллекции только элементов типа либо дочерних элементов этого типа
            var result27 = e.OfType<AdvanceEmployee>();

            var numbers09 = new System.Collections.ArrayList(4);
            numbers09.Add(1);
            numbers09.Add("two");
            numbers09.Add(3);
            numbers09.Add(4);
            var list09 = numbers09.OfType<int>().ToList();  //1 3 4


            /*                       Операции соединения двух колекции                 */


            // SQL Union объеденяет две коллекции в одну откидывая дубли! Masha - 1 шт
            var result28 = e.Union(Factory.GetEmployees2());

            //SQL UNION ALL                                               Masha - 2 шт
            var result29 = e.Concat(Factory.GetEmployees2());

            //SQL except все из левого множества кроме общих с правым множеством Masha - 0 шт
            var result30 = e.Except(Factory.GetEmployees2());

            //SQL Intersect - только общие элементы обоих множеств     Only Masha
            var result31 = e.Intersect(Factory.GetEmployees2());

            // Попробуюем найти имеются ли ошибки  в поле Name по типу русская С анлгийская С
            char[] charEngSimilarRus = "cCoOeEaATpPHKxXBM".ToCharArray();

            //точно затратнее
            var result32 = e.Count(x => x.Name.Intersect(charEngSimilarRus).Count() > 0);
            //должно быть быстрее, выведет количество полей имеющих хотя бы один символ из charEngSimilarRus
            var result33 = e.Count(x => x.Name.Intersect(charEngSimilarRus).Any());
            // объекты, выведет все объекты имеюшие ошибки в названиях полей
            var result34 = e.Where(x => x.Name.Intersect(charEngSimilarRus).Any());

            // нечто схожее с предыдущим, но имеет влияение на саму коллекцию
            IEnumerable<char> query1 = "Not what you might expect";
            foreach (char vowel in "aeiou") //C# 5.0 
                query1 = query1.Where(c => c != vowel);
            //Nt wht y mght xpct

            //А вот это поможет устранить косячные поля из примера charEngSimilarRus
            string[] names = { "Tom", "Dick", "Harry", "Mary", "Jay" };
            IEnumerable<string> query2 = names
                  .Select(n => n.Replace("a", "").Replace("e", "").Replace("i", "")
                  .Replace("o", "").Replace("u", ""))
                  .Where(n => n.Length > 2)
                  .OrderBy(n => n);
            //RESULT: { "Dck", "Hrry", "Mry" }

           

            string[] programmingLanguages = { "C#", "Java", "JavaScript", "Python", "PHP" };
            var result35 = programmingLanguages.Min(l => l.Length);//2

            //Aggregate Пример на понимание агрегирования элементов колекции
            int[] numbers = { 1, 2, 3, 4, 5 };

            var result36 = numbers.Aggregate((x, y) => 2 * x - y); //-25!!!
            /* 
                2 *   1 - 2 =   0,
                2 *   0 - 3 =  -3,
                2 *  -3 - 4 = -10,
                2 * -10 - 5 = -25.
            */

            // SequenceEqual
            var rose = new Flower() { Name = "Rose", Length = 70 };
            var tulip = new Flower() { Name = "Tulip", Length = 12 };
            var flowers1 = new List<Flower> { rose, tulip };
            var flowers2 = new List<Flower>
                                             {
                                                 new Flower { Name = "Rose", Length = 70 },
                                                 new Flower { Name = "Tulip", Length = 12 }
                                             };
            var result37 = flowers1.SequenceEqual(flowers2); //false 
            //Последовательности содержат идентичные данные,
            //но т.к. содержащиеся в них объекты имеют разные ссылки,
            //они не считаются равными
            // Если необходимо сравнить фактические данные объектов в последовательностях,
            // а не просто сравнивать их ссылки, необходимо реализовать IEqualityComparer<T>
            // универсальный интерфейс в своем классе.


            //Take - вернутт Н элементов  Skip SkipWhile Take While

            var result38 = e.Take(3);

            //Single - проверяет один ли элемент в колекции, если нет кидает исключение, выполнение сразу
            //var result40 = e.Single();


            /* Операции объеденения вножиств по ключю (расширение вправо)  */


            var result39a = e
                .Join(d, empls => empls.FK_Id_Department, deps => deps.id, (emps, deps) => new
                {
                    Department = deps.Name,
                    NumberOfPeople = deps.NuberOfPeople,
                    Employee = emps.Name
                });
            //Print(result39a);

            //Group группирует колекцию в  колекцию колекций, есть ряд преусмотренных выводов такие как IDictionary

            var result40 = result39a.GroupBy(x => x.Department);
            //foreach (var key in result40)
            //{
            //    Console.WriteLine(key.Key);
            //    foreach (var item in key)
            //    {
            //        Console.WriteLine($"{ item.Department,-10}|{ item.NumberOfPeople,-3}| P:{ item.Employee,-7}");
            //    }
            //}

            //более полезная перегрузка GroupBy Key+Result
            var result40a = result39a.GroupBy(x => x.Department,(resultSelectorKey,resultSelectorValue)=>
            new { Name = resultSelectorKey, CountInGroup = resultSelectorValue.Count() });
            //Print(result40a);

            //еще одна перегрузка GroupBy Key,ElementSelect,Result
            var result40b = result39a.GroupBy(x => x.Department
            ,elementSelector=> elementSelector.Employee
            ,(resultSelectorKey,resultSelectorValue)=> new 
            {Name = resultSelectorKey,
             valueList = resultSelectorValue.Aggregate((s,s2) => s+"|"+s2 ) 
            });
            //Print(result40b);

            //GropBy ToDictionary
            var result40c = result39a.GroupBy(x => x.Department).ToDictionary(g => g.Key, g => g.ToList());
            //foreach (var key in result40c)
            //{
            //    Console.WriteLine("GROUP -> "+key.Key);
            //    foreach (var item in key.Value)
            //    {
            //        Console.WriteLine($"{ item.Department,-10}|{ item.NumberOfPeople,-3}| P:{ item.Employee,-7}");
            //    }
            //}

            //объеденяет две полседовательности, не по ключу, а по итератору в колекции 
            var result41 = e.Zip(d,(first,second)=>first.Name + " " + second.Name);
            // ресзультат бред



        }

        private static void Print<T>(T input)
        {
            Console.WriteLine(input);
        }

        private static void Print<T>(T[] input)
        {
            foreach (var item in input)
            {
                Console.WriteLine(item);

            }
        }

        private static void Print<T>(IEnumerable<T> input)
        {
            foreach (var item in input)
            {
                Console.WriteLine(item);
            }
        }

        private static void Print(IEnumerable<Employee> input)
        {
            foreach (Employee item in input)
            {
                Console.WriteLine($"{item.Name,-10}|Age:{item.Age,-3}|P:{item.Payment,-7}|D:{item.StartTime,-11:yyyy.MM.dd}|idD_FK:{item.FK_Id_Department}");
            }
        }


        //сравнение синтаксиса
        static int[] GetPositive1(int[] array)
        {
            var list = new List<int>();
            foreach (var item in array)
            {
                if (item > 0) list.Add(item);
            }
            return list.ToArray();
        }
        //спец синтаксис LINQ запросов
        static int[] GetPositive2(int[] array)
        {
            var query = from item in array
                        where item > 0
                        select item;
            return query.ToArray();
        }
        //на делегатах, стелочных функциях и методах расширения, наиболее используемый
        static int[] GetPositive3_2(int[] array)
        {
            var query = array.Where(x => x > 0);
            return query.ToArray();
        }

        static int[] GetPositive3_1(int[] array) =>
            array.Where(x => x > 0).ToArray();

    }
}

/*LINQ(Language of INtegrated Queries) — язык запросов к наборам данных;
Реализован в виде методов, расширяющих коллекции (как правило, через IEnumerable<T>);
Для работы LINQ необходимо подключить через using пространство имён System.Linq;
Результатом каждого запроса является либо объект, либо его перечисление (IEnumerable<T>);
Существует две формы записи:
Специальный LINQ-синтаксис;
Standard Query Operators (обычные методы).
*/