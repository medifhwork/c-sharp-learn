﻿using System;
using System.Diagnostics;
using System.Threading;

namespace BubbleSort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = { 7, 2, 5, 3, 2, 1, 9, 9, 8, 0 };
            //var arrrnd = new int[1_000_000_000];
            //Action initarrAction = () => initArr(ref arrrnd);
            //~12.5s target 1_000_000_000
            //Measure(initarrAction);
            // PrintArr(arrrnd);
            //~11.4s AMD 4700U
            Action SortArrAction = () => BubbleSortClassic(ref arr);
            Measure(SortArrAction);
            PrintArr(arr);

        }

        private static void PrintArr(int[] arr)
        {
            foreach (var item in arr)
            {
                Console.Write(item + " ");
                //не плюсуй с чаром, тк срабатывает приведение символа к инт, а не строке
            }
            Console.WriteLine();
        }

        // сложность квадратичная, на малых размерах не критична
        private static int[] BubbleSortHack(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = i + 1; j < arr.Length; j++)
                {
                    if (arr[i] > arr[j])
                    {
                        arr[i] = arr[j] + arr[i];
                        arr[j] = arr[i] - arr[j];
                        arr[i] = arr[i] - arr[j];
                    }
                }
            }
            return arr;
        }


        private static void BubbleSortClassic(ref int[] arr)
        {
            int temp = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = i + 1; j < arr.Length; j++)
                {
                    if (arr[i] > arr[j])
                    {
                        temp = arr[i];
                        arr[i] = arr[j];
                        arr[j] = temp;
                    }
                }
            }

        }

        static void initArr(ref int[] arr)
        {
            Random r = new Random();

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = r.Next(0, 100);
            }
        }

        public static void Measure(Action callback)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            callback.Invoke();
            stopwatch.Stop();
            Console.WriteLine(callback.Method.Name + " : MS- " + stopwatch.ElapsedMilliseconds
                + " /Ticks- " + stopwatch.ElapsedTicks);
        }
    }
}
