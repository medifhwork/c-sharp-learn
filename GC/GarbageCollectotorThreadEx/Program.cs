﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace GarbageCollectotorThreadEx
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Main thread id:{0}",
                Thread.CurrentThread.ManagedThreadId);
            MyClass me = new MyClass();
        }
    }
    class MyClass
    {
        ~MyClass(){
            Console.WriteLine("Destructor thread ID:{0}",
                Thread.CurrentThread.ManagedThreadId ); 
        }
    }
}
