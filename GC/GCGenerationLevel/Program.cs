﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace CGGenerationLevel
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Система поддерживает {0} поколения.", (GC.MaxGeneration + 1));
            Console.WriteLine(new string('/', 40));

            NormalObject obj = new NormalObject();
           
            //паралельно набьем кучу другими большими обьектами
            new Thread(AuxiliaryMethod).Start();

            for (int i = 0; i < 300; i++)
            {
                Console.Write("Поколение: {0} | ", GC.GetGeneration(obj));
                Console.WriteLine("Размер кучи = {0} KB", GC.GetTotalMemory(false) / 1024); // true
                Thread.Sleep(100);
            }
            Console.WriteLine(new string('-', 40));
            Console.WriteLine("Поколение 0 проверялось {0} раз", GC.CollectionCount(0));
            Console.WriteLine("Поколение 1 проверялось {0} раз", GC.CollectionCount(1));
            Console.WriteLine("Поколение 2 проверялось {0} раз", GC.CollectionCount(2));
            Console.WriteLine(new string('-', 40));
        }

        static void AuxiliaryMethod()
        {
            OtherObject[] obj = new OtherObject[1000];//50*1000=50mb
            for (int i = 0; i < obj.Length; i++)
            {
                obj[i] = new OtherObject();
                for (int j = 0; j < obj[i].array.Length; j++)
                {
                   obj[i].array[j] = 0b0000_1111;

                    if (i % 2 == 0)
                        if (j % 3 == 0)
                            obj[i].array[j] = null;
                }
                Thread.Sleep(5);
                
            }
            for (int i = 0; i < obj.Length; i++)
            {
                for (int j = 0; j < obj[i].array.Length; j++)
                {
                    if (i - 1 % 2 == 0)
                        if (j - 1 % 3 == 0)
                            obj[i].array[j] = null;
                }
            }
        }
       
    }

    class NormalObject
    {
        byte[] array =  new byte[1024];  //1byte*1024 = 1024кб массив
        public NormalObject()
        {
            Console.WriteLine("this Constructor and my hash is {0}",this.GetHashCode());
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = 0b0000_1000;
            }
        }
        ~NormalObject () 
        {
            Console.WriteLine("this Destructor and my hash is {0}", this.GetHashCode());
        }
    }

    class OtherObject
    {
        public byte?[] array = new byte?[1024 * 50]; //50kb array
    }
}
