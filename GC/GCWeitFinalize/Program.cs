﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCWeitFinalize
// Для успешной работы метода GC.WaitForPendingFinalizers() - 
// требуется включить оптимизацию:  
// В свойствах проекта, вкладка Build -> группа General -> установить флаг Optimize Code.
{
    class MyClass
    {
        ~MyClass()
        {
            System.Threading.Thread.Sleep(200);
            Console.WriteLine("|finalize");
        }
    }

    class Program
    {
        static void Main()
        {

            for (int i = 0; i < 20; i++)
            { new MyClass(); }
            GC.Collect();
            GC.WaitForPendingFinalizers(); // Установить комментарий.

        }
    }
}
