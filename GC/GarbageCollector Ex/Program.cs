﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarbageCollector_Ex
{
    class Program
    {
        static void Main(string[] args)
        {
            //MyClass me = new MyClass();  
            //me = null;  // убиваем ссылкуу
            //GC.Collect(); // Вызов сборщика мусё программы вызывается деструктор


            //Dispose example ONE
            //MyClass2 me2 = new MyClass2();
            //me2.Dispose();  
            //Console.ReadKey();


            //Dispos example TWO
            //using   создает некий блок кода, по завершению кторого вызывается dispose()
            //using (MyClass2 me2 = new MyClass2()) 
            //{
            //    me2.DoWork();
            //}// не явный вызов метода dispose()


            //MyClass2 me2 = new MyClass2();
            //try
            //{
            //    me2.DoWork();
            //}
            //finally
            //{
            //    if (me2 is IDisposable && me2 != null)
            //    {
            //        me2.Dispose();
            //    }

            //}



            MyClass3 me3 = new MyClass3();
            try
            {
                me3.DoWork();
                //me3.Dispose();
                //me3.Dispose();
                //me3.Dispose();
                //me3.Dispose();
                //me3.Dispose();
                //me3.Dispose();
            }
            finally
            {
                //if (me3 is IDisposable && me3 != null)
                //{
                //    me3.Dispose();
                //}

            }

        }
    }


    public class MyClass3 : IDisposable
    {
        private bool _isDisposed = false;

        public void Dispose() //Подколочка в том, что без флага состояния вызываются оба метода dispose and distructor
        {
            Dispose(true);
            GC.SuppressFinalize(this); //НЕ ВЫЗЫВАТЬ ДИСТРУКТОР ГЫ!

        }
        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing) //заходит если вызван метод dispose
                {
                    Console.WriteLine("I am finalaze!!! workin with other object and my HASH: " +
                                            this.GetHashCode());
                }
                 _isDisposed = true; //заходит если вызван  деструктор обьекта ???
                                     //может в лекции ошибка как будто бы нужно положить в блок else.
                                     //хотя ни кто деструктор напрямую вызвать уже не может, тогда нормально
            }
        }

        ~MyClass3()
        {
            Dispose(false);
            //в данном контексте  паттерна нужно деструктор все равно реализовать, 
            // если GC сам займется обьектом  Dispose (bool) не отработает, выставится флаг, и управление вернется сюда!!!!!!!
            Console.WriteLine("It's the work of a destructor! and my HASH: " +
            this.GetHashCode());
        }

        public void DoWork()
        {
            Console.WriteLine("I've been working ");
            
        }
    }


    public class MyClass2 : IDisposable
    {
        public void Dispose() //Подколочка в том, что без флага состояния вызываются оба метода dispose and distructor
        {
            Console.WriteLine("I am finalaze workin with other object and my HASH: " +
            this.GetHashCode());
            Console.ReadKey();
        }
        ~MyClass2()
        {
            Console.WriteLine("It's the work of a destructor! and my HASH: " +
            this.GetHashCode());
        }

        public void DoWork()
        {
            Console.WriteLine("I've been working ");
            throw new Exception("generate exeption in working progress");
        }
    }

    public class MyClass
    {
        //Destructor
        ~MyClass() // не могут иметь  модификаторов доступа , аргументов, и быть статическими, ну очевидно ж
        {
            try
            {
                throw new Exception("Some exeption!");
               
            }
            catch (Exception exeptionvalue)
            {
                Console.WriteLine(exeptionvalue.Message);
                
            }
            finally
            {
                Console.WriteLine("Finallized this object!");
            }
                    
            
            //// Console.WriteLine("Hello from destructor");
            // применять для управляемой в ручную финализации работы с другими обьектами
            // там где критично корректное завершение
            //for (int i = 0; i < 40; i++)
            //{
           //     Console.WriteLine(i);
            //    System.Threading.Thread.Sleep(1000);
            //}
        }
    }
}
