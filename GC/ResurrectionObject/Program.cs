﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResurrectionObject
{
   
    internal sealed class SomeType
    {
        int counter;
        ~SomeType()
        {
            Console.WriteLine("Finalizer {0}", counter++);

            // В этом случае при вызове метода Finalize объекта SomeType ссылка на него
            // помещается в статическую переменную живого объекта (Program)  
            // и объект (SomeType) становится доступным из кода приложения. 
            // Теперь объект "воскресает", а сборщик мусора не принимает его за мусор.
            // Program.Instance = this;

            if (this.counter < 3)
                // Вызов ReRegisterForFinalize используется для повторного вызова деструктора.
                GC.ReRegisterForFinalize(this);//без флага билда оптимизация кода работает не так как предпологаешь
        }
        public SomeType()
        {
            this.counter = 0;
        }
    }
    class Program
    {
        // public static SomeType Instance { get; set; }
        //
        //public static int counter;

        static void Main()
        {

            SomeType st = new SomeType();
            st = null;
            for (int i = 0; i < 10; i++)
            {
                System.Threading.Thread.Sleep(1000);
                GC.Collect();
                Console.WriteLine("\t iteration {0}", i);
            }

            Console.ReadKey();

            // Отработает деструктор ~SomeType()
        }
    }
}

