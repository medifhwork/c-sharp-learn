﻿using System;
using System.Threading;

namespace Lesson3_Threads_1
{
    class Program
    {
        static void Main(string[] args)
        {
            File f1 = new File();
            Thread th1 = new Thread(DoSomthing);
            th1.Name = "One";
            th1.Start(f1); 
            Thread th2 = new Thread(DoSomthing);
            th2.Name = "Two";
            th2.Start(f1); 
           
        }

        static void DoSomthing (object o)
        {
            File file = o as File;
            for (int i = 0; i < 30; i++)
            {
                Console.WriteLine(Thread.CurrentThread.Name + "  " + i);    
            }
        }

        class File { }
    }
}
