﻿using System;
using System.Collections.Generic;

namespace Lesson4_Generic_1
{
    class Program
    {
        static void Main(string[] args)
        {   //полный вызов обобщенного метода
            PrintMass<int>(new int[]{});
            //<int> закрытие обобщения
            //сокрашенный вызов обобщенного метода
            PrintMass(new int[] { });  // вызвался не generic метод, вопрeки ожиданиям.
            PrintMass(new string[] { });  // вызвался generic метод
            //пеедать можно любой Тип
            PrintMass<Program>(new Program[] {new Program(), new Program() });
            Console.ReadKey();
        }


        /// <summary>|| 1 ||
        ///Oргумент не обязан быть T, мог быть string[], если не нужно строит логику на этом.
        ///Но если оргумент указан с типом Т, то он принимает тот же тип что и generic method
        ///обощение, Т может быть чем угодно, например SomeType
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="mass"></param>
        //generic method here
        static void PrintMass<T>(T[] mass) 
        {
            //можно узнать переданный тип
            Console.WriteLine(typeof(T));

            foreach (var item in mass)
            {
                Console.WriteLine(item);
            }

        }

                                   
        /// <summary> || 2 ||
        ///Oбощение поддерживается на уровне CLR, по этому метод c перегрузкой без обобщения
        ///по факту является отдельным перегруженным методом
        ///после добавления такого метода одновременно с обобщенным, нужно внимательно смотреть 
        ///как именно вызывается метод, сокрашенный формат вызова обощения не сработает.
        /// </summary>
        /// <param name="mass"></param>
        static void PrintMass(int[] mass)
        {
            Console.WriteLine("You shot in the leg");
        }
    }
}
