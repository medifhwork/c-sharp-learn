﻿using System;
using System.Collections.Generic;

namespace Lesson4_Generic_3_
{
    class Program
    {
        static void Main(string[] args)
        {
            //Storage<Figure> storage = new Storage<round>(); 
            // не возможно, вспоминаем правила обобщенных типов, закрытый тип является отельным классом
            // между фигурой и кругом нет ни каких наследсвенных связей.
            Console.WriteLine("ковариация");
            IFactory<Figure> factory1 = new RectadleFactory();
            Console.WriteLine("IFactory<Figure> = "+factory1.GetType().Name);

            Console.WriteLine("контрвариация");
            IStorage<Round> storage1 = new FigureStorage();
            Console.WriteLine("IStorage<Round> = "+storage1.GetType().Name);
            IStorage<Round> storage2 = new RoundStorage();
            Console.WriteLine("IStorage<Round> = " + storage2.GetType().Name);

        }
    }


    //Out T - указывает компилятору на использование Т - в ковариативном контексте
    public interface IFactory<out T> where T : Figure
    {
        T CreateFigure();

        //void Save(T obj);  не допустимо тк указано работать как T out. Компилятор закрывает возможный NRE
    }

    // In t - указывает компилятор на использование Т - в контрвариативном контексте
    public interface IStorage<in T> where T : Figure
    {
        void Save(T figure);
    }

    public class FigureStorage : IStorage<Figure>
    {
        public void Save(Figure figure)
        {
            Console.WriteLine("SAVED");
        }

        //T GetStorage() { }; низя
    }

    public class RoundStorage : IStorage<Round>
    {
        public void Save(Round figure)
        {
            Console.WriteLine("SAVED");
        }

        //T GetStorage() { }; низя
    }

    public class RoundFactory : IFactory<Round>
    {
        public Round CreateFigure() => new Round();
    }
    public class RectadleFactory : IFactory<Rectangle>
    {
        public Rectangle CreateFigure() => new Rectangle();
    }

    public class Figure
    {

    }
    public class Round : Figure
    {

    }
    public class Rectangle : Figure
    {

    }
}


//ВАРИАТИВНОТСЬ
//1 Проблема с несовместимостью обобщённых типов между собой решается вариативностью интерфейсов;
//2 Ковариация: обобщённый интерфейс с аргументом класса-предка может быть совместим с обобщённым
//      типом с аргументом класса-потомка, но не может принимать такие объекты;
//3 Контравариация: обобщённый интерфейс с аргументом класса-потомка может быть совместим с
//      обобщённым типом с аргументом класса-предка, но не может возвращать такие объекты.