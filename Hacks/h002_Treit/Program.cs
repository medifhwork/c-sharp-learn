﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treit
{
    class Program
    {
        static void Main(string[] args)
        {
            Penguin peng = new Penguin();
            peng.Swim();
            peng.Walk();
        }
    }
    //------------------------------------

    abstract class Trait    { }

    class CanWalk : Trait { }

    class CanSwim : Trait { }
    //------------------------------------
    interface ITreit<T> where T : Trait { }

    static class AnimalTraits
    {
        public static void Walk(this ITreit<CanWalk> trait)
        {
            Console.WriteLine("Walk");
        }
        public static void Swim(this ITreit<CanSwim> trait)
        {
            Console.WriteLine("Swim");
        }
    }
    //------------------------------------
    class Penguin : ITreit<CanSwim>, ITreit<CanWalk> { }
    //------------------------------------

}
