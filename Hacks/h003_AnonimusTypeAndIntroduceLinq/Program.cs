﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _003AnonimusType
{
    class Program
    {
        static void Main(string[] args)
        {
            var x = new  //это анонимный тип у него есть много ограничений см в конце
            {
                X = 1,
                Name = "Vasya",
                Age = 28
            };

            Console.WriteLine(x.GetType().Name);
            Console.WriteLine(x.Name);
            Console.WriteLine(x.Age);
            Console.WriteLine(x.X);


            Console.WriteLine("---------использование с листом--------------");
            Employee em0 = new Employee()
            {
                FirstName = "jj",
                LastName = "oo",
                Department = "A"
            };
            Employee em1 = new Employee()
            {
                FirstName = "qq",
                LastName = "oo",
                Age = 22,
                Department = "B"
            };
            Employee em2 = new Employee()
            {
                FirstName = "ee",
                LastName = "dd",
                Department = "A"
            };
            List<Employee> lstEmp = new List<Employee>();
            lstEmp.AddArrayParams(em0, em1, em2);
            PrintEmployee(lstEmp);
            Console.WriteLine("--------------------------------");
            var filteredEmp = GetEmployess(lstEmp,"A");
            //PrintEmployee(filteredEmp); хер
            foreach (var item in filteredEmp)
            {
                Console.WriteLine(item);//это жопа какая то
            }

            
        }

        private static void PrintEmployee(List<Employee> lstEmp)
        {
            foreach (Employee item in lstEmp)
            {
                Console.WriteLine($"Dep - {item.Department} FName - {item.FirstName}");
            }
        }

        public static List<object> GetEmployess(List<Employee> lst, string department)
        {
            var res = new List<object>();
            foreach (Employee employ in lst)
            {
                if (employ.Department == department)
                {
                    res.Add(new //упаковать анонимку можно только в родительский object
                    {
                        FN = employ.FirstName,
                        LN = employ.LastName,
                        Department = employ.Department
                    });
                }
            }
            return res;
        }
    }

    class Employee {

        public string Department;
        public string FirstName;
        public string LastName;
        public int Age;
        public bool HasChildren;
        public bool IsMarried;
    }

    public static class ExtentionList
    {
        public static void AddArrayParams<T> (this List<T> lst, params T[]  param )
        {
            foreach (var item in param)
            {
                lst.Add(item);
            }
        }
    }

}
/*Ограничения анонимных типов*/

/*Анонимный тип не может быть предком или потомком какого-либо класса.
Анонимный тип не может реализовать интерфейс.
Анонимные типы нельзя использовать в сигнатурах методов (как на вход, так и на выход)/ свойств и т.п.
 Коллекцию или массив анонимного типа объявить нельзя.
*/
/*При необходимости объекты анонимного типа можно сохранить в коллекцию/массив object`ов, но лучше так не делать: сильно усложняется доступ к его членам (только рефлексией или через dynamic).*/



