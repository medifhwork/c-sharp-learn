﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

namespace FastSplitString
{
    class Program
    {
        static string a;

        static void Main(string[] args)
        {
            StringBuilder s = new StringBuilder();
            Random r = new Random();
            s.Append((int)r.Next(0, 1000));
            for (int i = 1; i < 10000; i++)
            {
                if (i % 2 == 0)
                    s.Append((int)r.Next(0, 1000));
                else s.Append(' ');
            }
            a = s.ToString();
            //  Console.WriteLine(a);
            Action d1 = FastMetod;
            Action d2 = SlowMethod;
            Measure(SlowMethod);
            Measure(d2);
            Measure(d2);
            Measure(d1);
            Measure(d2);
            Measure(d2);
            Measure(d2);
            Measure(d1);
            Measure(d1);
            Measure(d1);
        }

        private static void FastMetod()
        {
            var a2 = a.SplitStrToIntEmun();
            foreach (int item in a2)
            {
                var t = item;
            }
        }

        private static void SlowMethod()
        {
            var a1 = a.Split(' ');
            foreach (string item in a1)
            {
                int.TryParse(item, out int t);
                //Console.WriteLine(t);
            }
        }

        public static void Measure(Action callback)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            callback?.Invoke();
            stopwatch.Stop();
            Console.WriteLine(callback.Method.Name + " : MS- " + stopwatch.ElapsedMilliseconds
                + " /Ticks- " + stopwatch.ElapsedTicks);
        }

    }

    public static class Utils
    {
        public static IEnumerable<int> SplitStrToIntEmun(this string input)
        {
            int number = 0;
            foreach (var ch in input)
            {
                if (char.IsWhiteSpace(ch))
                {
                    yield return number;
                    number = 0;
                    continue;
                }

                number = number * 10;
                number = number + (ch - '0');
                //
                //This works because each character is internally represented by a number.
                //The characters '0' to '9' are represented by consecutive numbers,
                //so finding the difference between the characters '0' and '2'
                //results in the number 2.

            }
            yield return number;
        }
    }
}
