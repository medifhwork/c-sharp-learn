﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtentionMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World ! ! !".CalcWhiteSpace());
        }
    }
    //расширенный метод заключается в добавлении в пространство имен проекта статического класса со статическим методом, в параметре которого указан расширяемый типа через ключевое слово this
    //после чего этот метод становится доступным для указанного типа в рамках сборки.
    public static class ExtentionMethod
    {
        public static int CalcWhiteSpace(this string value)
        {
            int counter = 0;
            for (int i = 0; i < value.Length; i++)
            {
                if (char.IsWhiteSpace(value[i]))
                {
                    counter++;
                }
            }
            return counter;
        }
    }
}
