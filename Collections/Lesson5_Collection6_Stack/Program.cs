﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson5_Collection6_Stack
{
    class Program
    {
        static void Main(string[] args)
        {//LIFO
            Stack<int> stack = new Stack<int>();
            stack.Clear();
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);
            stack.Push(4);
            stack.Push(5);

            var length = stack.Count;  //странное поведение, если его использовать в заголовке цикла происходит чертовщина напоминающая как будто оно в разных потоках.
            for (int i = 0; i < length; i++)
            {
                Console.WriteLine(stack.Pop());
            }//теперь тут пусто
            Console.WriteLine("------------");


            //stack.Clear();
            stack.Push(10);
            stack.Push(20);
            stack.Push(30);
            stack.Push(40);
            stack.Push(50);
            foreach (int item in stack) // доступ к стеку через IEnumerable не приводит к модификации данных стэка.
            {
                Console.WriteLine(item);
            }

            foreach (int item in stack)
            {
                Console.WriteLine(item);
            }


            Console.WriteLine("------------");
            Stack<Person> persons = new Stack<Person>();
            persons.Push(new Person() { Name = "Tom" });
            persons.Push(new Person() { Name = "Bill" });
            persons.Push(new Person() { Name = "John" });

            foreach (Person p in persons)
            {
                Console.WriteLine(p.Name);
            }
            Console.WriteLine("------------");
            // Первый элемент в стеке
            Person person = persons.Pop(); // теперь в стеке Bill, Tom
            Console.WriteLine(person.Name);

            Console.ReadLine();
        }
    }


    class Person
    {
        public string Name { get; set; }
    }


}
