﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson5_Collection1_3
{
    class Program
    {
        static void Main(string[] args)
        {
            HashSet<int> set = new HashSet<int>();
            set.Add(5); //5
            set.Add(5); //5
            set.Add(5); //5
            // Каждый элемент хэш множества должен быть уникальным.
            // Следовательно вставить дубли не получится
            Console.WriteLine(set.Count); //1

            //отберем уникальные элемнты в массиве при помоши хэш множества.
            List<int> lst = new List<int>() {1,1,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,3,4,5,6,7,7,7,8,9,9,9,9 };
            Console.WriteLine("List " + lst.Count); //29
            HashSet<int> setBylst =new HashSet<int>(lst) ; //1,2,3,4,5,6,7,8,9
            //можно было бы и в цикле отфилтровать вставляя в хэшсет, но зачем если есть перегрузка с IEnumarable
            Console.WriteLine("HashSet "+ setBylst.Count); //9
           


        }
    }
}


// ISet  не имеет реализации в шарпе, есть HeshSet