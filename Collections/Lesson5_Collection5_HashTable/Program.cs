﻿using System;
using System.Collections;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson5_Collection5_HashTable
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable ht = new Hashtable();
            ht.Add(1, "One");
            ht[1] = "oter";  //перезапись или добавление
            ht[2] = "oter2";  //перезапись или добавление
            ht[3] = "oter3";
            //ht.Add(1, "AnotherOne");//Исключение

            Console.WriteLine(ht[1]);
            Console.WriteLine("---------------");

            foreach (var item in ht)
            {
                Console.WriteLine(item);  //главный недостаток не имеет обобщенной реализации в отлчиии Dictionary
            }
            for (int i = 0; i < ht.Count+1; i++)
            {
                Console.WriteLine(ht[i]);
            }
        }
    }
}
