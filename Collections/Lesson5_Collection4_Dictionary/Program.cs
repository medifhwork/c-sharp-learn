﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson5_Collection4_Dictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            //IDictionary<int, string> dict = ..
            //Dictionary<int,string> dict = new Dictionary<int, string>() {new KeyValuePair<int,string>(1,"One")};

            Dictionary<string, int> ages = new Dictionary<string, int>();
            ages.Add("Sasha",24);
            ages.Add("Masha",27);
            ages.Add("Natasha",23);

            //ages.Add("Sasha", 30); //киет исключение  System.ArgumentException: An item with the same key has already been added.

            //var value1 = ages["SashaALALLALALA"]; // System.Collections.Generic.KeyNotFoundException: The given key was not present in the dictionary.

            var isPetya = ages.TryGetValue("Petya",out int PetyasAge);
            Console.WriteLine($"isPetya {isPetya} {nameof(PetyasAge)} {PetyasAge}");
            var isNatasha = ages.TryGetValue("Natasha", out int NatashasAge);
            Console.WriteLine($"isNatasha {isNatasha} {nameof(NatashasAge)} {NatashasAge}");

            Console.WriteLine(ages["Sasha"]);//24  выдасат значение

            foreach (var item in ages)
            {
                Console.WriteLine(item.ToString());//[key, value]
            }
            foreach (var item in ages)
            {
                Console.WriteLine("{0,10},{1,-2}",item.Key,item.Value);
            }

            //поинт в ages1["Peter"] = 270;  оно валидно и не равносильно ADD
            var ages1 = new Dictionary<string, int>();
            ages1.Add("Anton", 23);
            ages1.Add("Masha", 22);
            ages1.Add("Peter", 220);
            //ages1.Add("Peter", 320); Exeption An item with the same key has already been added.
            ages1["Peter"] = 270;  // перезапишет или добавит ключь значение
            Console.WriteLine(ages1["Peter"]);


        }

    }
}
