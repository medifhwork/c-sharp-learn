﻿using System;
using System.Collections;


namespace InterIEnumerable
{
    class UserCollection : IEnumerable, IEnumerator
    {
        //композиция  тут будет хранится данные коллекции
        private Element[] _elements = null;
        
        public UserCollection()//для упрощения жизни создадиим сразу данные для колекции 
        {
            _elements = new Element[5]{ new Element("A",1,10),
                                        new Element("B",2,20),
                                        new Element("C",3,30),
                                        new Element("D",4,40),
                                        new Element("E",5,50) };
        }

        int _position = -1; //указатель текущей позиции

        //далее реализация IEnumerator
        //  ______________________________________________________________
        // |                                                              |         
        public object Current => _elements[_position];

        public bool MoveNext()
        {
            if(_position < _elements.Length - 1)
            {
                _position++;
                return true;
            }
            else
            {
                Reset();
                return false;  //главные приницип перечисляемых колейций while MoveNext()
            }
        }

        public void Reset()
        {
            _position = -1;
        }
    // |_______________________________________________________________|         



        //далее реализация интерфейса IEnumerable
    //  ___________________________________________________________________
    // |                                                                   |         
        //public IEnumerator GetEnumerator()  //etc
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this as IEnumerator;
        }// то есть закроет все остальные методы, кроме методов IEnumerator 
    // |___________________________________________________________________|     
    }
}
