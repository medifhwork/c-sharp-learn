﻿using System;

namespace InterIEnumerable
{
    public  class Element
    {
        private string _name;
        private int _field1;
        private int _field2;

        public string Name { get => _name; set => _name = value; }
        public int Field1 { get => _field1; set => _field1 = value; }
        public int Field2 { get => _field2; set => _field2 = value; }

        public Element(string s, int a, int b)
        {
            _name = s;
            this._field1 = a;
            this._field2 = b;
        }

        public override string ToString()
        {
            return $"[Name:{Name} f1: {Field1} f2: {Field2}]";
        }
    }
}
