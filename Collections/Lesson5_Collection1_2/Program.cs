﻿using System;
using System.Collections;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*           ICollection<T>
public interface ICollection<T> : IEnumerable<T>, IEnumerable
{

    int Count { get; }

    bool IsReadOnly { get; }

    void Add(T item);
  
    void Clear();

    bool Contains(T item);
   
    void CopyTo(T[] array, int arrayIndex);
 
    bool Remove(T item);
*/

namespace Lesson5_Collection1_2
{
    class Program
    {
        static void Main(string[] args)
        {
            MyCollection<int> myc = new MyCollection<int>();
            
        }
    }

    public class Mothership
    {
        private ICollection<Ship> _ships;

        public Mothership(ICollection<Ship> ships)
        {//идея в том, что в конструктор можем предать ICollection шипов c минимально необходимым функционалом (кстати это композиция или агрегация?)
            Ships = ships; // в данном примере это агрегация, так коллекция кораблей создается где то в другом контексте. посоле уничтожения  материнского корабля,  кораблики остануться жить в колекции созданной где то в др месте.
        }

        public ICollection<Ship> Ships { get => _ships; private set => _ships = value; }
    }
    public class Ship
    {
        public Ship(string name)
        {
            Name = name;
        }

        public string Name { get; }

        public override string ToString() => ' ' + this.Name + ' ';
    }

    public class MyCollection<T>:ICollection<T>
    {
        public int Count => throw new NotImplementedException();

        public bool IsReadOnly => throw new NotImplementedException();

        public void Add(T item)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(T item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<T> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public bool Remove(T item)
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public struct Enumerator : IEnumerator<T> //структура реализует  интерфейс
        {
            public T Current => throw new NotImplementedException();

            object IEnumerator.Current => throw new NotImplementedException();

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public bool MoveNext()
            {
                throw new NotImplementedException();
            }

            public void Reset()
            {
                throw new NotImplementedException();
            }
        }
    }

}
