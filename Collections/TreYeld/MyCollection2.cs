﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TryYeld
{
    public class MyCollection2 : IEnumerable<Element>
    {
        public Element this[int indexElement]
        {
            get
            {
                if (indexElement > Length - 1) return null;
                return _elements[indexElement];
            }
        }

        public Element this[string stringIndexElement]
        {
            get
            {
               return  this[Array.FindIndex(_elements, x => x.Name == stringIndexElement)];
            }
        }

        private Element[] _elements = null;
        private int _position = -1;

        public MyCollection2()
        {
            _elements = new Element[5]
            {
                new Element("A",1,10),
                new Element("B",2,20),
                new Element("C",3,30),
                new Element("D",4,40),
                new Element("E",5,50)
            };
        }

        public int Length { get => _elements.Length; }

        public void Reset()
        {
            _position = -1;
        }

        public IEnumerator<Element> GetEnumerator()
        {
            while (true)
            {
                if (_position < Length - 1)
                {
                    _position++;
                    yield return _elements[_position];
                }
                else
                {
                    Reset();
                    yield break;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
