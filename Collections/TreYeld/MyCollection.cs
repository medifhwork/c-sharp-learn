﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TryYeld
{
    public class MyCollection : IEnumerable<Element>
    {
        private Element[] _elements = null;

        public MyCollection()
        {
            _elements = new Element[5]
            {
                new Element("A",1,10),
                new Element("B",2,20),
                new Element("C",3,30),
                new Element("D",4,40),
                new Element("E",5,50)
            };
        }

        public int Length { get => _elements.Length; }

        public IEnumerator<Element> GetEnumerator()
        {
            for (int i = 0; i < _elements.Length; i++)
            {
                yield return _elements[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
