﻿using System;
using System.Collections;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson5_Collection1_IEnumerable
{
    class Program
    {
        static void Main(string[] args)
        {
            //не поделу, с разбора домашек
            GetFirstValue(); //иницилизация многомерного масива и доступ к элементу

            IEnumerable<int> mass = new int[] { 4, 5, 6 };// масив интов положим в коллекцию  поддерживающую интерфейс IEnumerable  
            IEnumerable<int> list = new List<int> { 1, 2, 3 }; // лист положим в ссылку поддерживаюшую интерфейс IEnumerable  
            PrintMass(mass); //выведем в консоль через универсальный дженерик метод
            PrintMass(list);
            (list as List<int>).AddRange(mass);  // приведем list к изначальной ссылке List<int>  вомпользуемся методом AddRange - добавим к  листу массив
            PrintMass(list);
            Console.WriteLine("Count elems = " + (list as List<int>).Count);
            //Console.WriteLine("Count elems = " + (mass as List<int>).Count); Unhendled NRE

            /*долгая иницилизация
            List<Ship> listShips = new List<Ship>();
            listShips.Add(new Ship("Dunkan"));
            listShips.Add(new Ship("Novohudonosor"));
            listShips.Add(new Ship("Andromeda"));
            var ms = new Mothership (listShips);
            */
            // по быстрее
            //var ms = new Mothership(new List<Ship>() { new Ship("A"), new Ship("B") }); //  пришлось воспрользоваться листом, вот если бы ship был унаследован от IEnumerable
            var ms = new Mothership(new Ship[] //  но! подойдет любой поддерживающий IEnumerable
                                        {new Ship("Dunkan Idaho"),
                                         new Ship("Novohudonosor"),
                                         new Ship("Andromeda")}
                                   );
            PrintMass(ms.Ships);

            Console.WriteLine(new String('-',60));
            Foo(ms.Ships);
            Foo(list);

         
            void Foo<T>(IEnumerable<T> vls)   //вобше это частности и такой код бесмысленен. PrintMass лучше.
            {
                Console.Write(vls.GetType().Name + ": ");
                if (vls.Equals(ms.Ships))//только потому что не статика, так не дает написать IEnumerable < Ship >
                {
                    IEnumerable<Ship> ships1 = (IEnumerable < Ship > )vls;
                    foreach (var item in ships1)
                    {
                        Console.Write("{0,-15}",item.Name);
                    }
                } else
                {
                    foreach (var item in vls) Console.Write($"{item,-3}");
                }
                Console.WriteLine();
            }

        }

        private static void PrintMass<T>(IEnumerable<T> values) //не ожиданно но через интерфейс метод становится довольно гибким и юзабельным хмхм ням ням. 
        {
            Console.Write(values.GetType().Name + ": ");
            foreach (var item in values)
            {
                Console.Write($"{item,-3}");
            }
            Console.WriteLine();
        }

        static void GetFirstValue()
        {
            var arr = new int[3, 3, 3, 3, 3];
            (arr as Array).SetValue(999, 1, 1, 1, 1, 1);
            Console.WriteLine((arr as Array).GetValue(1, 1, 1, 1, 1));
        }

        public class MyCollerction<T> : IEnumerable<T>  //под капотом нужно будет реализовать как обобщенный  IEnumerator<T> так и обычный. Исторически в шарпе поддерживаются оба. 
        {
            public IEnumerator<T> GetEnumerator()  // Получение перечислителя на самом деле должно реализовываться созданием нового перечислителя, для потокобезопасности.
            {
                throw new NotImplementedException();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                throw new NotImplementedException();
            }
        }
    }

    public class Mothership
    {
        private IEnumerable<Ship> _ships;

        public Mothership(IEnumerable<Ship> ships)
        {//идея в том, что в конструктор можем предать лист шипов, массив шипов,  итд
            Ships = ships;
        }

        public IEnumerable<Ship> Ships { get => _ships; private set => _ships = value; }
    }
    public class Ship
    {
        public Ship(string name)
        {
            Name = name;
        }

        public string Name { get; }

        public override string ToString() => ' ' + this.Name + ' ';
    }
}


