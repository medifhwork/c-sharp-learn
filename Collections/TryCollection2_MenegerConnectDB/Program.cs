﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace foo00012312387654
{
    class Program
    {
        static void Main(string[] args)
        {

            DBS01 dbs01 = new DBS01();
            DBS02 dbs02 = new DBS02();
         
            ConnectionManager cm = new ConnectionManager(dbs01.GetConnect(),dbs02.GetConnect());
            
            foreach (Connect item in cm)
            {
                Console.WriteLine(item.ConnectionString +"is Main connect?  " +item.Equals(cm[0]));
            }

        }
    }


    public interface IConnect : IEquatable<IConnect>
    {
        string ConnectionString { get; }
    }  
        

    public class Connect  : IConnect
    {
        private string _connectionString;

        public string ConnectionString { get => _connectionString; private set => _connectionString = value; }

        public Connect(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public bool Equals(IConnect other)
        {
            return this.ConnectionString == other.ConnectionString;
        }

    }
    
    public interface IDeliverConnect
    {
         Connect GetConnect();
    }

    public  class DBS01 : IDeliverConnect
    {
        const string  MainConn = @"\\dbs01";
        public  Connect GetConnect() => new Connect(MainConn);
    }
    public class DBS02 : IDeliverConnect
    {
        const string SecondConn = @"\\dbs02";
        public  Connect GetConnect() => new Connect(SecondConn);
    }


    public class ConnectionManager 
    {
        private IConnect[] _connects = null;

        private int _countConnects = 0;

        private int _index = -1;

        public int CountConnects { get => _countConnects;}

        public IConnect this[int index]
        {
            get => index > -1 ? _connects[index] : throw new IndexOutOfRangeException();
        }

        private void Reset() => _index = -1;

        public IEnumerator<IConnect> GetEnumerator()
        {
            Reset();
            while (true)
            {
                if (_index < _countConnects - 1)
                {
                    _index++;
                    yield return _connects[_index];
                }
                else
                {
                    Reset();
                    yield break;
                }
            }
        }

        public bool AddConnect(IConnect connect) { throw new NotImplementedException(); }

        public bool RemoveConnect(IConnect index) { throw new NotImplementedException(); }

        public ConnectionManager (params IConnect[] cons)
        {
            if (cons == null || String.IsNullOrWhiteSpace(cons[0].ConnectionString))
                throw new ArgumentNullException();

            this._connects = new IConnect[cons.Length];
            this._countConnects = cons.Length;
            for (int i = 0; i < cons.Length; i++)
            {
                _connects[i] = cons[i] as IConnect;
            }
        }
    }
}
