﻿using System;

namespace FileSystemWatcher
{
    using System;
    using System.IO;

    namespace MyNamespace
    {
        class MyClassCS
        {
            public static  DateTime lastRead = DateTime.MinValue;


            static void Main()
            {
                using var watcher = new FileSystemWatcher(@"D:\watch");

                watcher.NotifyFilter =// NotifyFilters.Attributes
                                      //NotifyFilters.CreationTime
                                     //| NotifyFilters.DirectoryName
                                      NotifyFilters.FileName
                                     //| NotifyFilters.LastAccess
                                     | NotifyFilters.LastWrite
                                     //| NotifyFilters.Security
                                     //| NotifyFilters.Size
                                     ;

                watcher.Changed += OnChanged;
                //watcher.Created += OnCreated;
                //watcher.Deleted += OnDeleted;
                //watcher.Renamed += OnRenamed;
               // watcher.Error += OnError;
                

                watcher.Filter = "*.*";
                watcher.IncludeSubdirectories = false;
                watcher.EnableRaisingEvents = true;

                Console.WriteLine("Press enter to exit.");
                Console.ReadLine();
            }

            public static bool IsFileReady(string filePath)
            {
                try
                {
                    using (FileStream inputStream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.None))
                        return inputStream.Length > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }

            private static void OnChanged(object sender, FileSystemEventArgs e)
            {
                if (e.ChangeType != WatcherChangeTypes.Changed)
                {
                    return;
                }

                DateTime lastWriteTime = File.GetLastWriteTime(e.FullPath);
                if (lastWriteTime != lastRead)
                {
                    Console.WriteLine($"Changed: {e.FullPath}");
                    Console.WriteLine($"Changed Type: {e.ChangeType}");
                    Console.WriteLine(File.GetLastWriteTime(e.FullPath).ToString("HH_mm_ss_FFFF"));
                    Console.WriteLine("------------");
                    lastRead = lastWriteTime;
                }
                // else discard the (duplicated) OnChanged event


                
            }

            private static void OnCreated(object sender, FileSystemEventArgs e)
            {
                string value = $"Created: {e.FullPath}";
                Console.WriteLine(value);
            }

            private static void OnDeleted(object sender, FileSystemEventArgs e) =>
                Console.WriteLine($"Deleted: {e.FullPath}");

            private static void OnRenamed(object sender, RenamedEventArgs e)
            {
                Console.WriteLine($"Renamed:");
                Console.WriteLine($"    Old: {e.OldFullPath}");
                Console.WriteLine($"    New: {e.FullPath}");
            }

            private static void OnError(object sender, ErrorEventArgs e) =>
                PrintException(e.GetException());

            private static void PrintException(Exception? ex)
            {
                if (ex != null)
                {
                    Console.WriteLine($"Message: {ex.Message}");
                    Console.WriteLine("Stacktrace:");
                    Console.WriteLine(ex.StackTrace);
                    Console.WriteLine();
                    PrintException(ex.InnerException);
                }
            }

         
           
               
           
        }
    }
}
