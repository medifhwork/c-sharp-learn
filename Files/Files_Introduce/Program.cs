﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Collections;
using Newtonsoft.Json;
using System.Text;

namespace Files_Introduce
{
    class Program
    {
        static void Main(string[] args)
        {
            //PrintEvrimomentInfo();

            //PrintDriversInfo();

            //PrintDiretoriesInfo(@"D:\Download");

            //CreateCatalogsInPath(@"D:\Download");

            //PrintFileInfoInSomeDirectory("D:\\Download");

            //CreateFile();

            //CreateFileIn10YersAgo();

            //WriteBytesFromIntValue(int.MaxValue);

            //Console.WriteLine(GetValueFromBinaryFile());

            //CustomStreamCreate();

            //BinaryReaderAndBuffStream();

            //WriteFileWithStreamWriter();

            //ReadFileWithStreamReader();

            //SerialazeObjAndWriteFile();

            //DeSerialazeSpaceShipAtFile();

            //ReadFileWithUsingOperator();

            //ReadTextCharOneByOne();

       
        }

        private static void ReadTextCharOneByOne(string sourceFilePath = @"D:\testCSharpFile.txt")
        {
            // Create a FileStream object so that you can interact with the file system
            FileStream sourceFile = new FileStream(
                                     sourceFilePath, // Pass in the source file path.
                                     FileMode.Open,   // Open an existing file.
                                     FileAccess.Read);// Read an existing file.
            StreamReader reader = new StreamReader(sourceFile);
            StringBuilder fileContents = new StringBuilder();
            // Check to see if the end of the file has been reached.
            while (reader.Peek() != -1)
            {
                // Read the next character.
                fileContents.Append((char)reader.Read());
            }
            // Store the file contents in a new string variable.
            string data = fileContents.ToString();
            // Always close the underlying streams release any file handles.
            reader.Close();
            sourceFile.Close();

            Console.WriteLine(data);

        }

        //как организовать чтение массива байт из файла используя FS и BinaryReader
        private static void ReadBinaryFileExample(string sourceFilePath = @"D:\testCSharpFile.txt")
        {
            FileStream sourceFile = new FileStream(
                                   sourceFilePath,
                                   FileMode.Open,
                                   FileAccess.Read);
            BinaryReader reader = new BinaryReader(sourceFile);
            int position = 0;
            int length = (int)reader.BaseStream.Length;
            byte[] dataCollection = new byte[length];
            int returnedByte;
            while ((returnedByte = reader.Read()) != -1)
            {
                dataCollection[position] = (byte)returnedByte;
                position += sizeof(byte);
            }
            reader.Close();
            sourceFile.Close();
        }


        private static void ReadFileWithUsingOperator()
        {
            string filePath = @"D:\testCSharpFile.txt";
            // этото оператор вызывает метод dispose() у объекта заключенного в скобках
            //Предоставляет удобный синтаксис, обеспечивающий правильное использование
            //объектов IDisposable.Начиная с C# 8.0 инструкция using гарантирует правильное использование объектов IAsyncDisposable.
            using (var stream = new FileStream(filePath, FileMode.Open))
            {
                var bffStream = new BufferedStream(stream);
                var reader = new StreamReader(bffStream);
                var temp = reader.ReadToEnd();
                Console.WriteLine(temp);
            }
        }

        private static SpaceShip DeSerialazeSpaceShipAtFile()
        {
            string filePath = @"D:\testCSharpFile.txt";
            var stream = new FileStream(filePath, FileMode.Open);
            var bffStream = new BufferedStream(stream);
            var reader = new StreamReader(bffStream);
            var ship = JsonConvert.DeserializeObject<SpaceShip>(reader.ReadToEnd());
            reader.Close();
            if (ship == null)
            {
                throw new JsonSerializationException("with file " + filePath);
            }
            Console.WriteLine("Space ship name: {0} \n Armor: {1} \n HP:{2}", ship.name, ship.Armor, ship.HP);
            return ship;
        }

        private static void SerialazeObjAndWriteFile()
        {
            string filePath = @"D:\testCSharpFile.txt";
            var stream = new FileStream(filePath, FileMode.Create);
            var bffStream = new BufferedStream(stream);
            var writer = new StreamWriter(bffStream);
            //use Obj to Json
            writer.WriteLine(JsonConvert.SerializeObject(new SpaceShip()));
            writer.Close();
        }

        public class SpaceShip
        {
            public string name = "Ship";

            public int HP = 100;

            public int Armor = 1000;
        }

        private static void WriteFileWithStreamWriter()
        {
            string filePath = @"D:\testCSharpFile.txt";
            var stream = new FileStream(filePath, FileMode.Create);
            var bffStream = new BufferedStream(stream);
            var writer = new StreamWriter(bffStream);
            writer.WriteLine(new Object());
            writer.WriteLine(DateTime.Now);
            writer.WriteLine("Ахалай махалай");
            writer.Close();
        }

        private static void ReadFileWithStreamReader()
        {
            string filePath = @"D:\testCSharpFile.txt";
            var stream = new FileStream(filePath, FileMode.Open);
            var bffStream = new BufferedStream(stream);
            // Implements a System.IO.TextReader that reads characters from a byte stream in
            // a particular encoding.
            var reader = new StreamReader(bffStream);
            var temp = reader.ReadToEnd();
            Console.WriteLine(temp);
            reader.Close();
            stream.Close();
        }

        private static void BinaryReaderAndBuffStream()
        {
            string filePath = @"D:\testCSharpFile.txt";
            var stream = new FileStream(filePath, FileMode.Open);
            //обертка над стримом, позволяющая CLR буфферизировать данные из потока(нагружает озу)
            var bffStream = new BufferedStream(stream);
            //обертка над потоком, добавляет более удобные методы работы с потоком байтов
            //привычная работа с указателем позиции в потоке
            var reader = new BinaryReader(bffStream);
            var byteArr = new byte[bffStream.Length];
            for (int i = 0; i < reader.BaseStream.Length; i++)
                Console.Write(reader.ReadChar());
            //странное развлечение читать только чары или инты, без карты того что записано
            //не прочитаешь, с чарами еше ок, но инты и другие типы можно записать только программно
            //или через двоичные редакторы
        }

        private static void CustomStreamCreate()
        {
            string filePath = @"D:\testCSharpFile.txt";
            var stream = new FileStream(filePath,
                FileMode.OpenOrCreate,
                FileAccess.ReadWrite,
                FileShare.None);
            stream.Close();
        }

        private static int GetValueFromBinaryFile(string filePath = @"D:\testCSharpFile.txt")
        {
            var file = new FileInfo(filePath);
            if (!file.Exists)
                throw new FileNotFoundException(filePath + " file not found");
            var stream = new FileStream(filePath, FileMode.Open);
            var byteArr = new byte[stream.Length];
            for (int i = 0; i < stream.Length; i++)
                byteArr[i] = (byte)stream.ReadByte();
            var value = BitConverter.ToInt32(byteArr, 0);
            // Console.WriteLine(value);
            stream.Close();
            return value;

        }

        private static void WriteBytesFromIntValue(int value, string filePath = @"D:\testCSharpFile.txt")
        {
            //var file = new FileInfo(filePath);
            var stream = new FileStream(filePath, FileMode.Create);
            var bytes = BitConverter.GetBytes(value);
            stream.Write(bytes);
            stream.Close();
        }

        private static void CreateFileIn10YersAgo()
        {
            string filePath = @"D:\testCSharpFile.txt";
            var stream2 = new FileStream(filePath, FileMode.OpenOrCreate);
            stream2.Close();
            var file = new FileInfo(filePath);
            file.LastAccessTime = DateTime.Now.AddYears(-10);
            file.LastWriteTime = DateTime.Now.AddYears(-10);
        }

        private static void CreateFile()
        {
            string filePath = @"D:\testCSharpFile.txt";
            // create obj with filepath from continue actions with file
            var file = new FileInfo(filePath);
            // open stream for work with a file and create it file
            var stream = file.Create();
            // write 5 byte in file
            stream.Write(new byte[] { 1, 0, 1, 0, 1 }, 0, 5);
            // close stream, same thing CLOSE FILE
            stream.Close();
            //delete file
            file.Delete();

            string filePath2 = @"D:\testCSharpFile2.txt";
            //create stream throught new stream, enum FileMode:OpenOrCreate,CreateNew,Create,Truncate,Append
            var stream2 = new FileStream(filePath2, FileMode.OpenOrCreate);
            stream2.Close();
            //delete file
            File.Delete(filePath2);
        }

        private static void PrintFileInfoInSomeDirectory(string directory)
        {
            const long mb = 1024 * 1024;

            FileInfo[] filesInDownloadCatalog = new DirectoryInfo(directory).GetFiles();

            foreach (var file in filesInDownloadCatalog)
            {
                Console.WriteLine("Name: {0}", file.Name);
                Console.WriteLine("Size: {0}", file.Length / mb);
                Console.WriteLine("CreationTime: {0}", file.CreationTime);
                Console.WriteLine("LastWriteTime: {0}", file.LastWriteTime);
                Console.WriteLine("Attributes: {0}", file.Attributes);

            }
        }

        private static void CreateCatalogsInPath(string path)
        {   //уникальный 32 знаковый идентификатор
            Guid guid = Guid.NewGuid();
            //возвращает путь из массива параметров
            var newpath = Path.Combine(path, guid.ToString());
            //DirectoryInfo  не статический класс, для развернутой работы с каталогом
            DirectoryInfo di = new DirectoryInfo(newpath);
            try
            {
                di.Create();
                Console.WriteLine("Dir was created in: {0}", newpath);
                //Directory - статический класс, позволяющий выполнять разовые задачи 
                Directory.CreateDirectory(newpath + "\\1");
                Directory.CreateDirectory(newpath + "\\2");
                Directory.CreateDirectory(Path.Combine(path, guid.ToString(), "3"));
                //File - статический класс для работы с файлом
                //FileStream  -  класс для работы с двоичными данными в файле
                FileStream file1 = File.Create(newpath + "\\File1.txt");
                file1.Close();
                //bool  == True - принудительно удалить все содержимое рекурсивно кроме файлов, открытых для работы
                //di.Delete();// inherits exeption
                di.Delete(true);
                Console.WriteLine("Dir was deleted in: {0}", newpath);
            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }
        }

        private static void PrintDiretoriesInfo(string path)
        {
            DirectoryInfo RootDirDdrive = DriveInfo.GetDrives().Where(d => d.Name == "D:\\").FirstOrDefault().RootDirectory;

            DirectoryInfo selectedDirectory = new DirectoryInfo(path);
            if (selectedDirectory.Exists)
                foreach (var directory in selectedDirectory.GetDirectories())
                {
                    Console.WriteLine("Name: {0}", directory.Name);
                    Console.WriteLine("FullName: {0}", directory.FullName);
                    Console.WriteLine("Attributes: {0}", directory.Attributes);
                    Console.WriteLine("CreationTime: {0}", directory.CreationTime);
                    Console.WriteLine("Extension: {0}", directory.Extension);
                    Console.WriteLine("LastWriteTime: {0}", directory.LastWriteTime);
                    Console.WriteLine("LastAccessTime: {0}", directory.LastAccessTime);
                }
        }

        private static void PrintDriversInfo()
        {
            //Class DriveInfo uses only one static method GetDrives
            //And allows get DirectoryInfo obj root directory in each drive

            DriveInfo[] driveInfo = DriveInfo.GetDrives();

            const long gb = 1024 * 1024 * 1024;
            foreach (DriveInfo drive in driveInfo)
            {
                Console.WriteLine("Имя: {0}", drive.Name);
                Console.WriteLine("Певданим: {0}", drive.VolumeLabel);
                Console.WriteLine("Тип: {0}", drive.DriveType);
                Console.WriteLine("Готовность: {0}", drive.IsReady);
                Console.WriteLine("FileSystem: {0}", drive.DriveFormat);
                Console.WriteLine("FreeSpace~: {0}", drive.AvailableFreeSpace / gb);
                Console.WriteLine("Syze~: {0}", drive.TotalSize / gb);
                Console.WriteLine("RootDirectoryCreationTime: {0}", drive.RootDirectory.CreationTime);
            }
        }

        private static void PrintEvrimomentInfo()
        {
            Console.WriteLine("AppDomain.CurrentDomain.BaseDirectory: {0}", AppDomain.CurrentDomain.BaseDirectory);
            Console.WriteLine("AppDomain.CurrentDomain.FriendlyName: {0}", AppDomain.CurrentDomain.FriendlyName);
            Console.WriteLine("AppDomain.CurrentDomain.Id: {0}", AppDomain.CurrentDomain.Id);
            Console.WriteLine("CurrentDirectory: {0}", Environment.CurrentDirectory);
            Console.WriteLine("SystemDirectory: {0}", Environment.SystemDirectory);
            Console.WriteLine("UserName: {0}", Environment.UserName);
            Console.WriteLine("MachineName: {0}", Environment.MachineName);
            Console.WriteLine("VersionString: {0}", Environment.OSVersion.VersionString);
        }

    }
}


/*
*   Полезные данные об окружении

*AppDomain.CurrentDomain.BaseDirectory
*Environment.CurrentDirectory
*Environment.SystemDirectory
*Environment.UserName
*Environment.MachineName
*Environment.OSVersion.VersionString
*
*
*   Информация о файлах и каталогах
*DriveInfo
*Directory
*DirectoryInfo
*File
*FileInfo
*
*
*   Работа с файлами и каталогами
*File, Directory 
*   Статические классы. Используются в случае, когда нужно выполнить одно действие.
*FileInfo, DirectoryInfo
*   Используются в случае, когда предполагается выполнить несколько действий с одним файлом/каталогом.
*
*FileMode – режимы открытия файла
*           Append – Открывает существующий, или создает. Указатель в конец
*           Create – создает новый, перезаписывает старые
*           CreateNew – создает новый, если такой файл есть, то IOException
*           Open – открывает существующий
*           OpenOrCreate – открывает, или создает
*           Truncate – открывает существующий, и очищает его. Новый не создается
*FileAccess – режимы доступа к файлу для данного потока
*           Read
*           ReadWrite
*           Write 
*FileShare  – режимы доступа к файлу для остальных потоков
*           None 
*           Read
*           ReadWrite
*           Write 
*           Inheritable – доступ только потомкам, не поддерживается под Win32
*           
*           
*    Группы потоков       
*Классы для работы с двоичными данными:
*   FileStream
*   MemoryStream
*   BufferedStream
*Классы-оболочки:
*   BinaryReader
*   BinaryWriter
*Классы для работы с текстовыми данными:
*   StreamReader
*   StreamWriter 
*   StringReader
*   StringWriter 
*
*
*   Удобный доступ к данным 
*Быстрое чтение всего файла
*   File.ReadAllText()
*   File.ReadAllBytes()
*   File.ReadAllLines()
*Быстрая запись всего файла
*   File.WriteAllText()
*   File.WriteAllBytes()
*   File.WriteAllLines()
*/
