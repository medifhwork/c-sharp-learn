﻿using System;

namespace Tanks
{
    class Program
    {
        static void Main(string[] args)
        {
            Gun g1 = new Gun(12,3);
            Gun g2 = new Gun(14,4);
            
            Panzer p1 = new Panzer("G112", g1,14,100);
            Panzer p2 = new Panzer("G112",  g2, 11, 100);

            p2.Shoot();
            p2.LoadGun("кумулятивный");
            p2.Shoot();
        }
    }
}
