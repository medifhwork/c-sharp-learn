﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tanks
{
    public abstract class Ammo : ICloneable
    {
        Gun gun;  //field class - aggregations because is in constructor ref
        public string type;

        public Ammo(Gun someGun, string type)
        {
            gun = someGun;
            this.type = type;
        }

        public virtual int GetDamage()
        {
            //TO OVERRIDE: add logic of variable damage depending on Ammo type
            return gun.GetCaliber() * 3;
        }

        public int GetPenetration()
        {
            return gun.GetCaliber();
        }

        public override string ToString()
        {
            return $"Снаряд " + type + " к пушке калибра " + gun.GetCaliber();
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    public class HECartridge : Ammo  //фугасный снаряд
    {
        public HECartridge(Gun someGun) : base(someGun, "фугасный") { }

        public override int GetDamage()
        {
            return (int)(base.GetDamage());
        }
    }

    public class HEATCartridge : Ammo //кумулятивный снаряд
    {
        public HEATCartridge(Gun someGun) : base(someGun, "кумулятивный") { }

        public override int GetDamage()
        {
            return (int)(base.GetDamage() * 0.6);
        }
    }

    public class APCartridge : Ammo // подколиберный картридж
    {
        public APCartridge(Gun someGun) : base(someGun, "подкалиберный") { }

        public override int GetDamage()
        {
            return (int)(base.GetDamage() * 0.3);
        }
    }
}
