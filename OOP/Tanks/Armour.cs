﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tanks
{
    public abstract class Armour
    {
        public int thickness;
        public string type;

        public Armour(int thickness, string type)
        {
            this.thickness = thickness;
            this.type = type;
        }

        public virtual bool IsPenetrated(Ammo projectile)
        {
            return projectile.GetDamage() > thickness;
        }
    }


    public class HArmour : Armour
    {
        public HArmour(int thickness) : base(thickness, "гомогенная") { }
        public override bool IsPenetrated(Ammo projectile)
        {//благодаря свойсту полиморфизма объекта Ammo достаточно одного метода принимаюшего данный тип
            if (projectile is HECartridge)
            {
                //Если фугасный, то толщина брони считается больше
                return projectile.GetPenetration() > this.thickness * 1.2;
            }
            else if (projectile is HEATCartridge)
            {
                //Если кумулятивный, то толщина брони нормальная
                return projectile.GetPenetration() > this.thickness * 1;
            }
            else
            {
                //Если подкалиберный, то считаем уменьшаем толщину
                return projectile.GetPenetration() > this.thickness * 0.7;
            }
        }
    }

    public class SArmour : Armour
    {
        public SArmour(int thickness) : base(thickness, "гомогенная") { }
        public override bool IsPenetrated(Ammo projectile)
        {//благодаря свойсту полиморфизма объекта Ammo достаточно одного метода принимаюшего данный тип
            if (projectile is HECartridge)
            {
                //Если фугасный, то толщина брони считается больше
                return projectile.GetPenetration() > this.thickness * 0.8;
            }
            else if (projectile is HEATCartridge)
            {
                //Если кумулятивный, то толщина брони нормальная
                return projectile.GetPenetration() > this.thickness * 1.3;
            }
            else
            {
                //Если подкалиберный, то считаем уменьшаем толщину
                return projectile.GetPenetration() > this.thickness * 1;
            }
        }
    }


    public class CArmour : Armour
    {
        public CArmour(int thickness) : base(thickness, "гомогенная") { }
        public override bool IsPenetrated(Ammo projectile)
        {//благодаря свойсту полиморфизма объекта Ammo достаточно одного метода принимаюшего данный тип
            if (projectile is HECartridge)
            {
                //Если фугасный, то толщина брони считается больше
                return projectile.GetPenetration() > this.thickness * 1;
            }
            else if (projectile is HEATCartridge)
            {
                //Если кумулятивный, то толщина брони нормальная
                return projectile.GetPenetration() > this.thickness * 0.5;
            }
            else
            {
                //Если подкалиберный, то считаем уменьшаем толщину
                return projectile.GetPenetration() > this.thickness * 1.2;
            }
        }
    }


}
