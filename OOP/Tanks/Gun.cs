﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tanks
{
    public class Gun//оружие
    {
        private int caliber; //диаметр патрона
        private int barrelLength; //длинна ствола

        public Gun(int cal, int length) //конструктор
        {
            this.caliber = cal;
            this.barrelLength = length;
        }

        public int GetCaliber()
        {
            return this.caliber;
        }

        public bool IsOnTarget(int dice) //попадание в мишень dice - игральные кубики
        {
            return (barrelLength + dice) > 100;
        }
    }
}




