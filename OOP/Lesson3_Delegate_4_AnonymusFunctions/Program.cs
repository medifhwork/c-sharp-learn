﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Lesson3_Delegate_Anonimus
{
    public delegate int Function(int param);
    public delegate double MathFunction(double n1, double n2);

    class Program
    {
        //Существует две записи анонимных фунций
        //Анонимные методы
        static Function PlusOne = delegate (int x) { return x + 1; }; // принимает х возврашает х+1
                                //delegate {};  ни чего не принимает ни чего не возвращает пустышка.
        //Лямбда выражения
        static Function PlusOne1 = (x) => x + 1;
        static void Main(string[] args)
        {
            //var PlusOne1 = delegate (double x) { return x + 1; };// А так вот нельзя.
            //запишем еше короче при помощи стандартного делегата и анонимного метода delegate
            Func<int, int> PlusOne2 = delegate (int A) { return A+1; };
            //запишем еше короче при поmощи лямбда выражения и стандартного делегата
            Func<int, int> PlusOne3 = (x) => ++x; //забавно но x++ не вернет измененный х
            Do(PlusOne3);

            //как в итогде будет выглядеть преобразование массива из предыдущих уроков
            int[] arr = new int[] { 1, 2, 4, 8, 16, 32, 64, 128 };
            ApplyToArray4(arr, (randomName) => randomName * randomName);
            ViewArray(arr);
            //синтаксис с развернутым телом анонимной функции, в таком виде не нужно ни каких обьявлений зарание
            int[] arr2 = new int[] { 1, 2, 4, 8, 16, 32, 64, 128 };
            ApplyToArray4(arr2, (x) =>
            {
                var rnd = new Random();
                var tmp = rnd.Next(1, 99);
                return x * tmp;
            });
            ViewArray(arr2);
            //ключевое слово delegate часто используется в контексте аргумента, кторый ни чего не делает и ни чего не возвращает
            DoSomeLongTimeActions((f) => 
            {
                Console.WriteLine(f.GetType().Name + " is done!"); 
            });
            DoSomeLongTimeActions(delegate (File f) 
            {
                Console.WriteLine(f.GetType().Name + " is done to!"); 
            });
        } 



        public static void Do(Func<int, int> delegateReady)
        {
            int res =5;
            res = delegateReady(res);
            Console.WriteLine(res);
        }

        public static void ApplyToArray4<T>(T[] arr, Func<T, T> action) // 1T - returns 2T arguments
        {
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = action(arr[i]);
            }
        }

        public static void ViewArray(int[] ints)
        {
            Console.WriteLine();
            foreach (var item in ints)
            {
                Console.Write("{0,-6}", item);
            }
            Console.WriteLine();
        }

        static void DoSomeLongTimeActions(Action<File> action)//контрвариация
        {
            (int left, int top) curPos  = Console.GetCursorPosition();
            Console.Write("Loading..");
            for (int i = 0; i < 40; i++)
            {

                Console.SetCursorPosition(curPos.left+ 9, curPos.top+0);
                switch (i % 3)
                {
                    case 0: Console.Write("/"); break;
                    case 1: Console.Write("|"); break;
                    case 2: Console.Write(@"\"); break;
                }
                Console.SetCursorPosition(curPos.left + 9, curPos.top + 0);
                Thread.Sleep(5);
            }

            var file = new File();
            action.Invoke(file);
        }
        class File { }
      

        /// Outcome

        double EvalIntegral<T> //и так, одной из задач делегата, передача 
                               //подзадачи в задачу. Задача EvalIntegral
                               //Подзадача  Func<T, T> описана анонимным  стандартным делегатом
                               //принимаюшим Т параметров и возвращаюшем Т результат.
        (
            double a,
            double b,
            double eps,
            Func<T, T> func)
        {
            return 0d;
        }


        // вторую задачу которую решает delegate вызов Callback.
        public delegate void Callbak();  //вместо полной описании сигнатуры можно использовать 
        // стандартные делегаты, за частую так и делается.
        //.....тут N методов соответвующей сигнатуры с делегатом Callbak
        //наш метод с вызовом Callbak обработчика


        public void DoSomthingWork(Callbak callbak)  //модификатор доступа используемого делегата
            //и метода должен быть сопостовим.
        {
            //work...
            //callback?.invoke
            //важно вызывать делегат с проверкой на NRE
            //если передан multicast делегат содержащий более одного метода, 
            //произойдет почередный вызов всех методов в списке.
            //надо обращать внимание на иммутабильность переменных.
            // хм предыдущий пример не отличается своим механизмом от колбэка, передача задачи в задачу
        }

    }
}
