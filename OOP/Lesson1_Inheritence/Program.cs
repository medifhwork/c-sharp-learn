﻿using System;

namespace Lesson1_Inheritence
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            PointXY p1;
            p1 = new PointXY();
            PointXYZ p2 = new PointXYZ();
            PointXY p3 = new PointXY();
          
            p1 = GetPointXYZ();//ссылочная совместимость
            p1.Y = 0; //но не смотря на совместимость, получаем функционал левого операнда. Тк язык строго типизирован, думаю из за этого. 
                      //p1 объявленна как точка XY, это свойство не изменить, не положив по этой ссылке другой экземпляр.
            PointXY myPoint = GetPointXYZ(); //кастуется тоже ожидаемо благодаря правилу наследования Барбары Лисков
            PointXYZ myPoint3 = (PointXYZ)myPoint; //кастуется удачно, тк что хотели то и получили
            Console.WriteLine(myPoint3.Z);
            //теперь обратная ситуация, когда в обвертке базовый обьект, и приводим к дочернему 
            try  //хороший патерн если очень нужно ручное явное приведение 
            {
                PointXYZ myXYZ = (PointXYZ)GetPointXY();  //компилятор позволяет нам сделать явное преобразование
                                                          //но в рантайм мы получим ошибку кастования. не хватка данных для иницилизации расширенного функционала
                                                          //Unhandled exception. System.InvalidCastException: Unable to cast object of type 'Lesson1_.PointXY' to type 'Lesson1_.PointXYZ'.
            }
            catch (InvalidCastException)
            {
                Console.WriteLine("InvalidCastException");
            }
            Console.WriteLine(""); Console.WriteLine(); Console.WriteLine();

            // ОПЕРАТОР AS AS AS AS AS AS AS AS AS AS AS AS AS AS AS AS AS AS AS AS AS AS AS AS AS AS AS AS AS AS AS AS AS AS AS AS AS AS
            PointXYZ myXYZ_ = GetPointXY() as PointXYZ;  //что же получится используя полупопие (AS)
            Console.WriteLine("myXYZ_ as PintXYZ: "+myXYZ_);//получаем Null без ошибки RUNTIME
            // ОПЕРАТОР IS IS IS IS IS IS IS IS IS IS IS IS IS IS IS IS IS IS IS IS IS IS IS IS IS IS IS IS IS IS IS IS IS IS IS IS IS IS 
            if (myXYZ_ is PointXYZ)
            {
                Console.WriteLine(myXYZ_.Z);
                Console.WriteLine((bool)(myXYZ_ is PointXYZ));
            }
            else
            {
                Console.WriteLine("нЕ является myXYZ_ объектом класса PointXYZ");
                Console.WriteLine(myXYZ_ is PointXYZ);
            }
            //Console.WriteLine(myXYZ_.Z); //System.NullReferenceException in RunTime!!!

            if (p2 is PointXYZ)
            {
                Console.WriteLine("Является P2 объектом класса PointXYZ P.Z="+p2.Z);
            }
            else
            {
                Console.WriteLine("нЕ является myXYZ_ объектом класса PointXYZ");
            }
            //Проверка типа
            PointXYZ point123;
            point123 = myXYZ_ as PointXYZ;
            if (point123 != null)
            {
                // do somthing
            }
            PointXY point124;
            if (GetPointXYZ() is PointXY)
            {
                point124 = (PointXY)(GetPointXYZ());
            }
            Console.WriteLine(new String('+',120));

            PointXY someObject = GetPointXYZW();// восходящий каст
            Console.WriteLine(someObject.GetType().Name); //PointXYZW  типа будет неявно скастованный

        }
        static PointXY GetPointXY()
        {
            return new PointXY() { X = 1, Y = 2 };
        }
        static PointXYZ GetPointXYZ()
        {
            return new PointXYZ() { X = 1, Y = 2, Z = 3 };
        }
        static PointXYZW GetPointXYZW()
        {
            return new PointXYZW() { X = 1, Y = 2, Z = 3, W = 4};
        }
    }
    //наследование
    class Human
    {

    }
    class Worker : Employee
    {

    }
    class Employee : Human
    {

    }
    class VectorXY
    {

    }
    class PointXY
    {
        public int X;
        public int Y;
        private VectorXY ConvertToVectorXY()
        {
            return null;
        }

    }
    class PointXYZ : PointXY
    {
        public int Z;
    }
    class PointXYZW : PointXYZ
    {
        public int W;
    }

}
