﻿using System;
using System.Threading;

namespace Lesson3_Delegate_2
{

    public delegate void FileCallBack(File file);

    class Program
    {
        
        static void Main(string[] args)
        {
           // DoSomeLongTimeActions(FileDownloadingHandler);
            DoSomeLongTimeActions(FileDownloadingHandler2);
        }

        static void FileDownloadingHandler(File file)
        {
            Console.WriteLine(file.GetType().Name+" is done!");
        }

        static void FileDownloadingHandler2(File file)
        {
            Console.WriteLine(file.GetType().Name + "is succesfully loaded at your local disk!");
        }

        static void DoSomeLongTimeActions(FileCallBack callBack)//контрвариация
        {
            Console.Write("Loading..");
            for (int i = 0; i < 40; i++)
            {
                
                Console.SetCursorPosition(9, 0);
                switch (i % 3)
                {
                    case 0: Console.Write("/"); break;
                    case 1: Console.Write("|"); break;
                    case 2: Console.Write(@"\"); break;
                }
                Console.SetCursorPosition(9, 0);
                Thread.Sleep(50);               
            }

            var file = new File();                     
            callBack.Invoke(file);
        }
        
    }

    public class File
    {

    }

   
}
