﻿using System;

namespace Lesson3_Event_1_entry
{//Событие — это именованный делегат, при вызове которого, будут запущены все подписавшиеся на момент вызова события методы заданной сигнатуры.
    class Program
    {//Объявление и генерация события

        public event EventHandler<EventArgs> Came;
        //EventHandler и EventHandler<T>  делегаты из стандартных

        public delegate void EventHandler(object sender, EventArgs e);  //стандартная сигнатура события не передающего данные
        public delegate void EventHandler<T>(object sender, T e);//Если событие создает данные, необходимо использовать универсальный 

        protected virtual void OnCame()  // генератор события
        {
            if (Came != null)
                Came(this, EventArgs.Empty);
        }


        static void Main(string[] args)
        {
            TestEvent test = new TestEvent();
            test.Came += Test_Came;  //подписка на событие event Came которое по сути является делегатом с особым синтаксимом
            test.Came += Test_Came2;
            test.Came += Test_Came3;
            test.Came += newTestLissener;            
            //вызовем исскуственно событие
            test.CallOnCame();
            //все три подписчика получили срабатывание события
        }

        private static void Test_Came1(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private static void newTestLissener(object sender, EventArgs e)
        {
            Console.WriteLine("test");
        }

        private static void Test_Came(object sender, EventArgs e)  //автогенерируемое IDE  обраточик события
        {
            // throw new NotImplementedException();
            Console.WriteLine("hello");
        }

        private static void Test_Came2(object sender, EventArgs e)  //автогенерируемое IDE  обраточик события
        {
            // throw new NotImplementedException();
            Console.WriteLine("hello2");
        }

        private static void Test_Came3(object sender, EventArgs e)  //автогенерируемое IDE  обраточик события
        {
            // throw new NotImplementedException();
            Console.WriteLine("hello3");
        }
    }

    public class TestEvent : IEmpoyee
    {//Событие инкапсулирует объект делегата, как свойство инкапсулирует поле
        public event EventHandler<EventArgs> Came;
        //Oбъект делегата не создаётся.Т.е.для хранения списка подписчиков придётся самостоятельно организовать структуру хранения(объект группового делегата или их коллекцию).

        //EventHandler и EventHandler<T>  делегаты из стандартных
        //public delegate void EventHandler(object sender, EventArgs e);
        //public delegate void EventHandler<T>(object sender, T e);

        //При явной реализации интерфейса(с указанием его имени) событие должно быть реализовано в развёрнутом виде.
        event System.EventHandler<EventArgs> IEmpoyee.Came
        {
            add //могут быть назначенны дополнительные обработчики при подписке  
            {
                throw new NotImplementedException();
            }

            remove// соответвенно и  отписке
            {
                throw new NotImplementedException();
            }
        }

        protected virtual void OnCame()
        {
            if (Came != null)
                Came(this, EventArgs.Empty);
        }

        public void CallOnCame()
        {
            OnCame();
        }
    }

    interface IEmpoyee  //события могут быть бьявленны в интерфейсе
    {
        event EventHandler<EventArgs> Came;
    }

}
