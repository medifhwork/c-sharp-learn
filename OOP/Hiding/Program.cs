﻿using System;

namespace Hiding
{
    class Program
    {
        static void Main(string[] args)
        {
            Main mn = new Slave();
            //mn. X DoBar() так же не доступен, вызов по типу объявленной переменной
            mn.DoFoo(); //ни чего не обычного отработал метод Main

            //Slave slWithMain = new Main(); //нарушает ковариативность

            Slave sl = new Slave();
            sl.DoBar(); //доступен и так же вызвав в теле метод предка получаем функционал предка.
            (sl as Main).DoFoo(); // отработал как предок


            Console.WriteLine(new String('-', 60));



            D d = new D();
            C c = d;
            B b = c;
            A a = b;
            d.M();
            c.M();
            b.M();
            a.M();


            #region еше пример
            Console.WriteLine(new String('+', 60));

            //Разница проявляется в случае полиморфизма.Если вы работаете с экземпляром класса-наследника через его родительский класс,
            //    то в случае, если вы будете вызывать переопределенный виртуальный метод(override), то будет вызвана его реализаци
            //я из наследника, а если перекрытый(new), то будет вызван метод базового класса.Нагляднее будет увидеть на примере:

            Console.WriteLine("-> Base derivedAsBase = new Derived();");
            Base derivedAsBase = new Derived();

            derivedAsBase.First();  // Derived.First(), выведет "First from Derived"                
            derivedAsBase.Second(); // Base.Second(), выведет "Second from Base"

            Console.ReadLine();

            Console.WriteLine("-> Derived derivedAsDerived = new Derived();");

            Derived derivedAsDerived = new Derived();

            derivedAsDerived.First();                 
            derivedAsDerived.Second(); 

            Console.ReadLine();


            Console.WriteLine("-> derivedAsDerived as Base;");

            var castedDerivedToBase = derivedAsDerived as Base;

            castedDerivedToBase.First();
            castedDerivedToBase.Second();

            Console.ReadLine();

            Console.WriteLine("Derived derived_ = new Base(); --не имеет смыса из за нарушения принципа ковариативности классов");
        }
    }
    class Base
    {
        public virtual void First()
        {
            Console.WriteLine("First from Base");
        }

        public virtual void Second()
        {
            Console.WriteLine("Second from Base");
        }
    }

    class Derived : Base
    {
        public override void First()
        {
            Console.WriteLine("First from Derived");
        }

        public new void Second()
        {
            Console.WriteLine("Second from Derived");
        }
    }


#endregion


    public class Main
    {
        protected void DoBar()
        {
            Console.WriteLine("MainBar");
        }

        public void DoFoo()
        {
            Console.WriteLine("MainFoo");
        }
    }

    public class Slave : Main
    {
        public new void DoBar()
        {
            Console.WriteLine("this at Slave Bar...unprotected metod call in base class");
            base.DoBar();
        }

        public new void DoFoo()
        {
            Console.WriteLine("this at Slave Foo");
            base.DoFoo();
        }
    }

    // сокрытие, позволяет заменить  метод  в обход  разрешений на переопределение, вызов осушествляется по ссылке  переменной, а не по объекту в который она ведет.
    // если родительский метод скрыт для внешних потребителей выташить его можно, вызвав в сокрытом методе.



    class A
    {
        public virtual void M() { Console.WriteLine("A"); }
    }
    class B : A
    {
        public override void M() { Console.WriteLine("B"); }
    }
    class C : B
    {
        new public virtual void M() { Console.WriteLine("C"); }
    }
    class D : C
    {
        public override void M() { Console.WriteLine("D"); }
    }


}

