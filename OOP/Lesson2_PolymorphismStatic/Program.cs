﻿using System;

namespace Polymorphism
{
    class Program
    {
        static void Main(string[] args)
        {
            var farm = new Farm();
            Animal animal = farm.GetAnimal(); // new Leo()
            animal.Voice(); //.. I Am a Leo// интересно, войс базового животного, то есть хоть по факту создан лео,  метод вызываетс родителя.
            Console.WriteLine(animal.GetType()); //LEO
            Leo leo = (Leo)animal;
            Console.WriteLine(((Leo)animal).GetType());
            leo.Voice(); //RRRR! I am a Leo
            //странно вот тут не понятно, и до каста и после в объекте type Leo, но в одном случае вызывается базовый метода в другом нет. То есть По сути не базовый метод, а этот медо лежит одновременно с другим в типе Leo?
            /*
            if (animal is Cat)
                (animal as Cat).Voice();
            else if (animal is Dog)
                (animal as Dog).Voice();
            else if (animal is Leo)
                (animal as Leo).Voice();
            else animal.Voice();
            */
            Console.ReadKey();
        }
    }

    public class Farm
    {
        public Animal GetAnimal()
        {
 //           return new Animal("a Animal");
 //           return new Cat("a Cat");
 //          return new Dog("a Dog");
            return new Leo("a Leo");
        }
    }
    public class Animal
    {
        protected string _name;

        public string Name { get => _name; private set => _name = value; }

        public void Voice()
        {
            Console.WriteLine("...  I Am " + Name);
        }

        public Animal(string name)
        {
            Name = name;
        }
    }

    public class Leo : Animal
    {
        public new void Voice() // new в этом случаи указывает вам и другим, что вы осознанно воспользовались перекрытием! если его убрать ни чего не изменится
        {
            Console.WriteLine("RRR! I Am "+Name);
        }
        public Leo(string name) : base(name)
        {
        }
    }

    public class Cat : Animal
    {
        public new void Voice()
        {
            Console.WriteLine("MeyMey! I Am " + Name);
        }
        public Cat(string name) : base(name)
        {
        }
    }

    public class Dog : Animal
    {
        public new void Voice()
        {
            Console.WriteLine("GrowGrowGrow! I Am " + Name);
        }
        public Dog(string name) : base(name)
        {
        }
    }
}
