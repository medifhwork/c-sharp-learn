﻿using System;
using System.Collections.Generic;

public class Program
{
    public static void Main(string[] args)
    {
        List<int> list = new List<int>();

        for (int i = 1; i <= 100; i++)
        {
            list.Add(i);
        }


        List<int> result1 = list.FindAll(

            delegate (int x)
            {
                return (x % 2 == 0); 
            });


        List<int> result2 = list.FindAll(

            x => x % 3 == 0

        );

        foreach (var item in result1)
        {
            Console.WriteLine(item);
        }

        foreach (var item in result2)
        {
            Console.WriteLine(item);
        }
    }
}