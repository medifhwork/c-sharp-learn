﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3_Delegate_BattleShips
{
    class Program
    {
        static void Main(string[] args)
        {
            var fightersBase1 = new MotherShip();

            var fightersBase2 = new MotherShip();

            var fighter1 = new Fighter(fightersBase1);
            var fighter2 = new Fighter(fightersBase1);
            var fighter3 = new Fighter(fightersBase1);

            fightersBase1.AddFighter(fighter1, fighter2, fighter3);

            fightersBase2.AddFighter(fighter2, fighter3);

            fighter2.HP -= 110;

            fightersBase1.PrintAll();
            Console.WriteLine("---");
            fightersBase2.PrintAll();

            Console.ReadKey();
        }
    }


    public class MotherShip
    {
        private List<Fighter> _fighters;

        public MotherShip()
        {
            _fighters = new List<Fighter>();
        }

        public void AddFighter(Fighter fighter)
        {
            if (fighter == null || _fighters.Contains(fighter))
                throw new ArgumentException
                    ($"The fighter: {fighter.GetHashCode()} already added in Mothership");
            {
                _fighters.Add(fighter);
                fighter.OnDestroy += OnFighterDestroyHandler;
            }
        }

        private void OnFighterDestroyHandler(Fighter fighter)
        {
            _fighters.Remove(fighter);
        }

        public void AddFighter(params Fighter[] item)
        {
            if (item != null)
            {
                foreach (var fighter in item)
                {
                    AddFighter(fighter);
                }

            }
        }

        public void RemoveFighter(Fighter fighter)
        {
            if (!_fighters.Contains(fighter))
                throw new ArgumentException("Incorrrect fighter ", nameof(fighter));
            {
                _fighters.Remove(fighter);
                fighter.OnDestroy -= OnFighterDestroyHandler;
            }
        }

        public void PrintAll()
        {
            foreach (var item in _fighters)
            {
                Console.WriteLine(item);
            }
        }
    }


    public class Fighter
    {
        public event Action<Fighter> OnDestroy = delegate { }; //() => { } или delegate {}; сразу закидывает пустой метод, устраняя NRE


        public void Destroy()
        {
            Console.BackgroundColor = ConsoleColor.Red;
            Console.WriteLine($"Fighter<{this.GetHashCode()}> get critical damage - BOOOM!!!");
            Console.BackgroundColor = ConsoleColor.Black;
            OnDestroy?.Invoke(this);
        }
        private int _hp;

        public Fighter(MotherShip baseShip)
        {
            HP = 100;
        }

        public int HP
        {
            get { return _hp; }
            set
            {
                _hp = value;
                if (HP <= 0) Destroy();                
            }
        }

        public override string ToString()
        {
            return $"Fighter<{this.GetHashCode()}> = {HP}hp";
        }
    }

}

