﻿using System;

namespace Lesson2_AbstractClass
{
    class Program
    {
        static void Main(string[] args)
        {
            Figure f = new Round();
            (f as Round).Radius = 10;
            Console.WriteLine( f.Area );

            //у абстрактного класса может быть статический конструктор для статический полей
            // конструткор с параметрами и без
            //публичиный либо защищенный
            string a = Animal2.Name;
            Console.WriteLine(a);

        }
    }


    abstract  public class Figure
    {
        abstract public double Area { get; }

        protected Figure() { } //правило хорошего тона
        

    }

    public class Round : Figure
    {
        public int Radius { get; set; }

        public override double Area
        {
            get { return Math.PI * Math.Pow(Radius, 2); }
        }

        public Round()
        { 
        }
    }


    public abstract class Animal2
    {
        static private string name;  //если абстрактный член класса является статическим он может быть private
        static Animal2()
        {
            Name = "static";
        }

        protected Animal2(int a)
        {
        }

        public static string Name { get => name; set => name = value; }
    }
}
