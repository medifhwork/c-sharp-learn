﻿using System;

namespace indexators
{
    class Program
    {
        static void Main(string[] args)
        {
            MyString str = new MyString(new char[] { 'A','b','N'});
            Console.WriteLine(str[2]);
            str[2] = 'Л';
            Console.WriteLine(str[2]);
            Console.WriteLine(str.ToString()); // но теперь и ту стринг надо переопределить)
        }
    }

    public class MyString
    {
        private char[] _symbols;

        public MyString(char[] symbols)
        {
            _symbols = symbols;
        }


        /*Индексаторы позволяют индексировать объекты и обращаться к данным по индексу.
         * Фактически с помощью индексаторов мы можем работать с объектами как с массивами.
         * По форме они напоминают свойства со стандартными блоками get и set, которые возвращают и присваивают значение.
         * возвращаемый_тип this [Тип параметр1, ...]
                {
                    get { ... }
                    set { ... }
                }
         */

        //индексатор это вот это
        public virtual char this[int id] //ключевое слово this используется как указание что это индексатор переопределяя []  
            // также может быть виртуальным
        {
            get { return _symbols[id]; } //как бы наш класс  это массив обьектов, задав индексатор мы даем доступ к элементам массива
            set { _symbols[id] = value; }
        }
    }
   
}
