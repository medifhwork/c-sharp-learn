﻿using System;
using System.Threading;
using System.Collections.Generic;

namespace Lesson3_Delegate_3
{
    public delegate double Function(double x); //это делегат, лучший аналог в делфи указатель на метод.
    public delegate double MathFunction(double n1, double n2); //Делегат нужно воспринимать как контейнер над методом, для перетаскивания метода между типами.
    public delegate void FileCallBack(File file); // etc Action<File>
    public delegate bool IsSomthing(int A); // etc Predicate<int>
  
   
    internal class Program
    {
        static double _a = 11;
        static double _b = 14;
        static double _resultadd = 0;

        private delegate void ArrayProcessor(double[] param); // в нутри класса
        

        //var SomeDelegate2 = new Action<int, int, int>(delegate { }); какая то лабуда

        static void Main(string[] args)
        {
            #region simple delegate
            /*
                MathFunction func = new MathFunction(Multiply); //иницилизация делегата с конкретным методом.
                DoSomeAction(func);

                DoSomeAction(new MathFunction(Sum));  //иницилизация делегата по слабой ссылке
                DoSomeAction(Multiply); // сахарная иницилизация
         


                func.Invoke(100, 3); //явный вызов метода который содержит делегат. В данном случае Multiply
                Console.WriteLine(func.Method.Name + ' ' + func.Invoke(100, 3));

                DoSomeAction(null);

                Console.WriteLine(new String('-', 60));
                var mass = new int[5] { 1, 2, 4, 8, 16 };
                Calculator.ViewArray(mass);

                Calculator.ApplyToArray(mass, new Calculator.IntAction(Calculator.Pow2)); //в данном контексте избыточно полное обьявление, см ниже, можно просто передать имя функции соответвующей сигнатуры
                Calculator.ViewArray(mass);

                mass = new int[5] { 1, 2, 4, 8, 16 };  //иммутабильность досвидания.
                Calculator.ApplyToArray(mass, Calculator.Pow3);
                Calculator.ViewArray(mass);
            */
            #endregion simple delegate

            #region multicastDelgate

            //multicast delegate
            /*
            MathFunction multycastDelegate;// = new MathFunction(Sum); 
            MathFunction sum = new MathFunction(Sum);
            MathFunction multiply = new MathFunction(Multiply);
            multycastDelegate = (MathFunction)Delegate.Combine(sum,multiply);
           
            DoSomeAction(multycastDelegate);
            Console.WriteLine("_resultadd = " + _resultadd);
            */
            // что то в этом духе но нихуя не работает,  точнее работает, так что  в точке обращения к делегату вызывыется только 1

            Action<File> callBack = new Action<File>((f) => { Console.WriteLine("Первый готов"); });
            callBack += (f) => Console.WriteLine("Второй готов");
            callBack += FileDownloadingHandler;
            callBack += (f) => Console.WriteLine("Третий готов");
            callBack += FileDownloadingHandler;
            callBack -= FileDownloadingHandler;  //отписать можно только именнованные делегаты
      

            DoSomeLongTimeActions2(callBack);

            #endregion

            #region standart delegate in declared in Delegate

            //Все входные аргументы обладают контравариацией, а возвращаемые — ковариацией
            //(методы имеют право принимать объекты - предки и возвращать объекты - потомки).


            //DoSomeLongTimeActions2(FileDownloadingHandler2);  // подкинули метод соответвуюший сигнатуре Action<File> или  сигнатере делегата FileCallBack
            /*
            Console.WriteLine(new String('-', 60));
            var mass = new int[5] { 1, 2, 4, 8, 16 };
            Calculator.ViewArray(mass);
            //подкинули метод соответвуюей сигнатуре стандартного делегата Func<T,T> ,где первое Т - return параметр
            // второе T аргумент. int Pow2(int a) == Func<T,T>
            Calculator.ApplyToArray2(mass, Calculator.Pow2); 
            Calculator.ViewArray(mass);
            */



            #endregion
        }

        static void FileDownloadingHandler(File file)
        {
            Console.WriteLine(file.GetType().Name + " is done!");
        }

        static void FileDownloadingHandler2(File file)
        {
            Console.WriteLine(file.GetType().Name + "is succesfully loaded at your local disk!");
        }

        static void DoSomeLongTimeActions(FileCallBack callBack)//контрвариация
        {
            (int left, int top) curPos = Console.GetCursorPosition();
            Console.Write("Loading..");
            for (int i = 0; i < 40; i++)
            {

                Console.SetCursorPosition(curPos.left + 9, curPos.top + 0);
                switch (i % 3)
                {
                    case 0: Console.Write("/"); break;
                    case 1: Console.Write("|"); break;
                    case 2: Console.Write(@"\"); break;
                }
                Console.SetCursorPosition(curPos.left + 9, curPos.top + 0);
                Thread.Sleep(50);
            }

            var file = new File();
            callBack?.Invoke(file);  //обязательно делать проверку перед вызовом на null либо чрез безопасное разименование.
            //может быть ситуация когда в ходе работы программы произошла отписка делегата, или всех делегатов, в таком случае привет NPE
        }

        static void DoSomeLongTimeActions2(Action<File> callBack)//контрвариация
        {
            Console.Write("Loading..");
            for (int i = 0; i < 40; i++)
            {

                Console.SetCursorPosition(9, 0);
                switch (i % 3)
                {
                    case 0: Console.Write("/"); break;
                    case 1: Console.Write("|"); break;
                    case 2: Console.Write(@"\"); break;
                }
                Console.SetCursorPosition(9, 0);
                Thread.Sleep(5);
            }

            var file = new File();
            callBack.Invoke(file);
        }




        static void DoSomeAction(MathFunction function)
        {
            //var result = 0d;
            //if (function != null)    // без Invoke
            //    result = function(5, 8);
            //result = function?.Invoke(5, 8) ?? -1.0;//но не совпадение типов Nullable double and double, выйти из ситуации поможет ?? но нет, если в делегат метод DoSomeAction передан null все равно будет NPE
            //выход следющий
            Console.WriteLine("a = "+_a +" b = " +_b);
            var result = function(_a, _b);//если в ни чего нет то переменная null метод врайтлайн корректно отработает, но не в случае если вы обратились к полям  делегата.
            _resultadd = _resultadd +(double)result;
            Console.Write(function?.Method?.Name + ' ' ?? "Null at param this method" + ' ');
            Console.WriteLine(result);
           // Console.WriteLine("_resultadd = "+ _resultadd);
        }

        static double Multiply(double n1, double n2) => n1 * n2;  //на стрелочной фигне

        //static double Multiply2(double n1, double n2)   // по олдовому
        //{
        //    return n1 * n2;
        //}

        static double Sum(double n1, double n2) => n1 + n2;

    }

    public class Calculator
    {
        public delegate int IntAction(int value);
        private static IntAction _currentIntAction;
        
        public static IntAction CurrentIntAction { get => _currentIntAction; }

        public static int Pow2(int a) => a * a;
        public static int Pow3(int a) => a * a * a;
        public static void ApplyToArray(int[] ints, IntAction action)
        {
            _currentIntAction = action;

            for (int i = 0; i < ints.Length; i++)
            {
                ints[i] = action(ints[i]);
            }
        }

        public static void ApplyToArray2<T>(T[] arr, Func<T,T>action) // 1T - returns 2T arguments
        {
            if (action == null)
                throw new ArgumentNullException(nameof(action));
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = action.Invoke(arr[i]);
            }
        }

        public static void ApplyToArray3<T>(int[] arr, Func<int, int> action) // 1T - returns 2T arguments
        {
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = action(arr[i]);
            }
        }

        public static void ViewArray(int[] ints)
        {
            Console.WriteLine(CurrentIntAction?.Method?.Name ?? "Native array");
            foreach (var item in ints)
            {
                Console.Write("{0,-6}", item);
            }
            Console.WriteLine();
        }
    }

    public class File { }

}
