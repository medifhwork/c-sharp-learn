﻿using System;

namespace Properties
{
    class Program
    {
        static void Main(string[] args)
        {
            SomeClass s = new SomeClass(); 
            Console.WriteLine(s.MyProperty);
            s.Ch();
            Console.WriteLine(s.MyProperty);
            //s.MyProperty = 3;
        }
    }

    public class SomeClass
    {
        public int MyProperty { get; private set; } = 23;

        public void Ch ()
        {
            MyProperty = 99;
        }
            
    }
}
