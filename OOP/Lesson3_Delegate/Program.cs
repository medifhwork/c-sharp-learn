﻿using System;

namespace Lesson3_Delegate
{
    public delegate double Function(double x); //это делегат, лучший аналог в делфи указатель на метод.
    public delegate double MathFunction(double n1, double n2); //Делегат нужно воспринимать как контейнер над методом, для перетаскивания метода между типами.

    internal class Program
    {
        private delegate void ArrayProcessor(double[] param); // в нутри класса

        static void Main(string[] args)
        {
            MathFunction func = new MathFunction(Multiply); //иницилизация делегата с конкретным методом.
            func -= Multiply;
            DoSomeAction(func);

            DoSomeAction(new MathFunction(Sum));

            func.Invoke(100, 3); //явный вызов метода который содержит делегат. В данном случае Multiply
            Console.WriteLine(func.Method.Name + ' ' + func.Invoke(100, 3));

            DoSomeAction(null);

            Console.WriteLine(new String('-', 60));
            var mass = new int[5] { 1, 2, 4, 8, 16 };
            Calculator.ViewArray(mass);

            Calculator.ApplyToArray(mass, new Calculator.IntAction(Calculator.Pow2)); //в данном контексте избыточно полное обьявление, см ниже, можно просто передать имя функции соответвующей сигнатуры
            Calculator.ViewArray(mass);

            mass = new int[5] { 1, 2, 4, 8, 16 };  //иммутабильность досвидания.
            Calculator.ApplyToArray(mass, Calculator.Pow3);
            Calculator.ViewArray(mass);
        }

        static void DoSomeAction(MathFunction function)
        {
            //var result = 0d;
            //if (function != null)    // без Invoke
            //    result = function(5, 8);
            //result = function?.Invoke(5, 8) ?? -1.0;//но не совпадение типов Nullable double and double, выйти из ситуации поможет ?? но нет, если в делегат метод DoSomeAction передан null все равно будет NPE
            //выход следющий
            var result = function?.Invoke(5, 8);//если в ни чего нет то переменная null метод врайтлайн корректно отработает, но не в случае если вы обратились к полям  делегата.

                Console.Write(function?.Method?.Name?? "Null at param this method");
                Console.Write(" ");
                Console.WriteLine(result);
        }

        static double Multiply(double n1, double n2) => n1 * n2;

        static double Multiply2(double n1, double n2)
        {
            return n1 * n2;
        }

        static double Sum(double n1, double n2) => n1 + n2;

    }

    public class Calculator
    {
        public delegate int IntAction(int value);
        private static IntAction _currentIntAction;

        public static IntAction CurrentIntAction { get => _currentIntAction; }

        public static int Pow2(int a) => a * a;
        public static int Pow3(int a) => a * a * a;
        public static void ApplyToArray(int[] ints, IntAction action)
        {
            _currentIntAction = action;

            for (int i = 0; i < ints.Length; i++)
            {
                ints[i] = action(ints[i]);
            }
        }
        public static void ViewArray(int[] ints)
        {
            Console.WriteLine(CurrentIntAction?.Method?.Name??"Native array");
            foreach (var item in ints)
            {
                Console.Write("{0,-6}", item);
            }
            Console.WriteLine();
        }
    }
}
