﻿using System;

namespace Lesson1_Boxing
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello Boxing!");

            int A = 1;

            int B = 1;

            Console.WriteLine(A==B); //true
            // is it BOXING
            Console.WriteLine("is it Boxing");
            object Aobj = A;
            Object Bobj = B;
            Console.WriteLine(Aobj == Bobj); //false
                   Bobj = A;
            Console.WriteLine(Aobj == Bobj ); //false
                                                          // это опирация дорогая.  Выделаяется память под новый обьект.

            //is IT UNBOXING
            Console.WriteLine("is it UnBoxing");
            int castedA = (int)Aobj;
            Console.WriteLine(castedA);
            int? castedB = Bobj as int?; // с оператором аз только с налбл типом совместимо
            Console.WriteLine(castedB);

            Console.WriteLine(new string('-',110).Insert(60,"exampl"));
            PrintToConsole(Bobj); //распаковка
            PrintToConsole(B); //упаковка
            PrintToConsole(1); //упаковка
            Console.WriteLine(1); //нет упаковки потому что вызывается перегруженный метод кокретно для type int
            Console.WriteLine("Hello "+ 5); //упакока строки и структуры (string left, object right)  5->object

            IDisposable disp = new Pont(); //тоже упаковка
        }

        public static void PrintToConsole(object o)
        {
            Console.WriteLine(o);
        }
    }
    public class Pont:IDisposable
    {
        int X;
        int Y;
    }
    interface IDisposable//кстати перекрыли оригинальный
    {

    }
}
 