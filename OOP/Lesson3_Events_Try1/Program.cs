﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Lesson3_Events_Try1
{
    class Program
    {
        static void Main( string[] args )
        {
            Counter counterobj = new Counter();
            Handler_I handler1 = new Handler_I();
            Handler_II handler2 = new Handler_II();
            //Теперь укажем событию onCount, методы, которые должны запуститься.
            //Происходит это следующим образом: < КлассИлиОбъект >.< ИмяСобытия > += < КлассЧейМетодДолженЗапуститься >.< МетодПодходящийПоСигнатуре >.
            //Никаких скобочек после метода!Мы же не вызываем его, а просто указываем его название.
            counterobj.onCount += handler1.Message;
            counterobj.onCount += handler2.Message;// подписались еба еба
            //запустим отсчет
            counterobj.Count();
        }
    }


    class Counter
    {
        public delegate void MethodConteiner();
        //Синтаксис по сигнатуре метода, на который мы создаем делегат: 
        //delegate <выходной тип> ИмяДелегата(<тип входных параметров>);
        //Мы создаем на void Message(). Он должен запуститься, когда условие выполнится.


        //public event MethodConteiner onCount;
        //public Action onCount; //ниче не сломалось)
        public EventHandler onCount;
        //Событие имеет синтаксис: public event <НазваниеДелегата> <НазваниеСобытия>;

        public void Count()
        {
            Console.Write("progress");
            (int left, int top) pos;
            for (int i = 0; i < 100; i++)
            {
                pos.left = Console.CursorLeft; pos.top = Console.CursorTop;
                PrintProgress(i, pos);                
                if (i == 71)
                {
                    //onCount(); //Все. Событие создано. Методы, которые вызовет это событие,
                    //определены по сигнатурам и на основе их создан делегат.
                    //Событие, в свою очередь, создано на основе делегата.
                    if (onCount != null) // рантайме можно умышенно получить делегат не ссылающийся ни на один метод, по этому будь аккуратен во имя ктулху
                        onCount(this, EventArgs.Empty);                   
                }
            }
            Console.SetCursorPosition(0,6);
        }

        private static void PrintProgress(  int i,(int left, int top) pos)
        {
            Console.CursorVisible = false;
            Console.SetCursorPosition(9,0);
            switch (i % 3)
            {
                case 0: Console.Write(".     ");
                    break;
                case 1:Console.Write(". . ");
                    break;
                case 2:Console.Write(". . .");
                    break;
            }
            Thread.Sleep(50);
        }
    }

    class Handler_I
    {
        public void Message()
        {
            Console.WriteLine();
            Console.WriteLine("реaкция на собыитие издателя у хендлер1");
        }
        public void Message( object sender, EventArgs e )
        {
            Console.WriteLine();
            Console.WriteLine("реaкция на собыитие издателя у подписчика хендлер1 - вызвал срабатывани события " + sender.GetType().Name);
        }
    }

    class Handler_II
    {
        public void Message()
        {
            Console.WriteLine();
            Console.WriteLine("реaкция на собыитие издателя еба еба я тоже получил. сказал хендлер2");
        }

        public void Message(object sender,EventArgs e)
        {
            Console.WriteLine();
            Console.WriteLine("реaкция на собыитие издателя еба еба я тоже получил. сказал хендлер2 - "+ sender.GetType().Name);
        }
    }
}
/*
Заключение.

Постарайтесь понять смысл и порядок создания события.
1. Определите условие возникновения события и методы которые должны сработать.
2. Определите сигнатуру этих методов и создайте делегат на основе этой сигнатуры.
3. Создайте общедоступное событие на основе этого делегата и вызовите, когда условие сработает.
4. Обязательно (где-угодно) подпишитесь на это событие теми методами, которые должны сработать и сигнатуры которых подходят к делегату.
*/


/*
Пару слов о .NET-событиях. Microsoft упростила задачу конструирования делегатов: .NET предлагает готовый делегат EventHandler и т.н. 
    «пакет» входных параметров EventArgs. Желаете событие? Берете готовый EventHandler, определяетесь в параметрах, «запихиваете» их в класс, 
    а класс наследуете от EventArgs. А дальше — как по расписанию)
*/