﻿using System;

namespace _003_Interfaces_8._0
{
    class Program
    {
        static void Main(string[] args)
        {
            IHuman human = new Human() { Name = "John" };
            human.SayHello();
            ((IHuman)new Human()).SayHello();
            ((IHuman)new Human()).SayHello();
            // Hello, I am John
            ((IFriendlyHuman)new Human()).SayHello();
            // Greeting, my name is John
        }

        public interface IHuman
        {
            string Name { get; set; }

            void SayHello()
            {
                Console.WriteLine($"Hello, I am {Name}");
            }
        }
        public interface IFriendlyHuman : IHuman
        {
            void IHuman.SayHello()
            {
                Console.WriteLine($"Greeting, my name is {Name}");
            }
        }
        public class Human : IHuman, IFriendlyHuman
        {
            public string Name { get; set; }
            void SayHello()
            {

            }
        }

    }

    

}
