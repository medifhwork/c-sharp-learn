﻿using System;

namespace Lesson2_Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            //Не явная реализация
            MyString str = new MyString();
            str.Print();

            //Явная реализация IPrintable
            IPrintable str1 = new MyString();
            str1.Print();

            //Явная реализация IShowable
            IShowable str2 = new MyString();
            str2.Print();

            //IShowable showable = new IShowable();//создать экземпляр интерфейса нельзя как и абстрактного класса.

           

            Console.ReadKey();
        }
    }

    public interface IPrintable
    {
        public delegate void MyEventDelegate(object sender, EventArgs e);

        void Print();
        private static int _listCount = 60;  //c#8.0

        static IPrintable() //c#8.0
        {
            _listCount = 0;
        }
    }

    public interface IShowable
    {
        static int someNum = 5;
        void Print();
    }

    public class MyString : IPrintable, IShowable
    {

        public void Print()  //если не указать  явно какой интерфейс, этот метод реализует оба интерфейса IPrintable, IShowable
        {                    //чаще всего используется не явная реализация! А явная не имлиментруется вовсе.
            Console.WriteLine("Не явная реализация");
        }

        void IPrintable.Print()  //Явная реализация
        {
            Console.WriteLine("Явная реализация IPrintable");
        }

        void IShowable.Print()  //Явная реализация
        {
            Console.WriteLine("Явная реализация IShowable");
        }
    }


    }
