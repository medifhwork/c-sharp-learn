﻿using System;

namespace Lesson2_PolymorphismDynamic
{
    class Program
    {
        static void Main(string[] args)
        {
            var farm = new Farm();
            Animal animal = farm.GetAnimal(); // new Dog()
            animal.Voice(); //GrowGrowGrow I Am a Dog
            (animal as KavkazkayaOvcharka).Voice();  //GrowwRRRRGrow  i am a Dog Arra,  new virtual требует обращение по ссылке
            Console.WriteLine(animal.GetType()); //LEO

            KavkazkayaOvcharka palkan = new KavkazkayaOvcharkaWarriorsSupport("Rex"); // не обычно здесь не трубуетя статическая типизация, тк кавказкая овчарка прямой предок
            palkan.Voice();

        }
    }

    public class Farm
    {
        public Animal GetAnimal()
        {
            //return new Animal("a Animal");
            //return new Cat("a Cat");
            //return new Dog("a Dog");
            //return new Leo("a Leo");
            //return new KavkazkayaOvcharka("A Dog Arra");
            return new KavkazkayaOvcharkaWarriorsSupport("Palkan");
        }
    }
    public class Animal
    {
        protected string _name;

        public string Name { get => _name; private set => _name = value; }   // поля если они указаны как virtual  можно переобределять!

        public virtual void Voice()
        {
            Console.WriteLine("...  I Am " + Name);
        }

        public Animal(string name)
        {
            Name = name;
        }
    }

    public class Leo : Animal
    {
        public override void Voice()
        {
            Console.WriteLine("RRR! I Am " + Name);
        }
        public Leo(string name) : base(name)
        {
        }
    }

    public class Cat : Animal
    {
        public override void Voice()
        {
            Console.WriteLine("MeyMey! I Am " + Name);
        }
        public Cat(string name) : base(name)
        {
        }
    }

    public class Dog : Animal
    {
        public override void Voice() ///sealed запрешает дальнейшую перегрузку но не знапрещает  делать сатическую перегрузку new virtual
        {
            Console.WriteLine("GrowGrowGrow! I Am " + Name);
        }
        public Dog(string name) : base(name)
        {
        }
    }

    public class KavkazkayaOvcharka : Dog
    {
        public KavkazkayaOvcharka(string name) : base(name)
        {
        }

        public new virtual void Voice()  //новая ветка методов Voice он не оверрайдженый в этом классе через статическую типизацию попасть сюда только можно лишь мой юный падаван
        {
            Console.WriteLine("GrowRRRRRGrow! I Am " + Name);
        }
    }

    public class KavkazkayaOvcharkaWarriorsSupport : KavkazkayaOvcharka
    {
        public KavkazkayaOvcharkaWarriorsSupport(string name) : base(name)
        {
        }

        public override void Voice()  //новая ветка методов Voice он не оверрайдженый в этом классе через статическую типизацию попасть сюда только можно лишь мой юный падаван
        {
            base.Voice();
            //base.base.Voice(); //увы нельзя
            Console.WriteLine("Grow Ser, my capitan Ser! I Am " + Name);
        }
    }
}

