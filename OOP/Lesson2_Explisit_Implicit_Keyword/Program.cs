﻿using System;

//перегрузка операций преобразования типа
//implicit operator - не явный каст
//explicit operator - явный каст
//implicit|explicit - комбинированный вариант
namespace Explisit_Implicit_Keyword
{
    class Program
    {
        static void Main(string[] args)
        {
            Vector vector = new Point();    //vector    || 1 ||
                Console.WriteLine(vector);
            //vector.x err нет такого поля
            Point point = (Point)vector;    //point     || 2 ||
                Console.WriteLine(point);
            Console.WriteLine(point.X);     //0
        }
    }


    //определяем правило Не явного преобразования.   || 1 ||
    public class Vector
    {
        public static implicit operator Vector(Point point)
        {
            return new Vector();
        }

    }


    
    public class Point
    {
        public int X { get; private set; }
        public int Y { get; private set; }

        //вызов перегрузки с 2я аргументами
        public Point() : this(0, 0) { }
       
        
       public Point(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
        //определяем правило Явного преобразования.  || 2 ||
        public static explicit operator Point(Vector v)
        {
            return new Point();
        }
    }



}
