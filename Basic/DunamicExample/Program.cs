﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DunamicExample
{
    class Program
    {
        static void Main(string[] args)
        {
            dynamic some = 5;

            some = "String";

            some = 4.5f;
            Single s;
            Console.WriteLine(some.GetType());  //System.Single
            Console.WriteLine(Single.IsInfinity(some));
            //минус первый  не работает помощник кода, тк  это рантайм типизация          
        }
    }
}
