﻿using System;

namespace EnumExample
{
    class Program
    {/// <summary>
     /// это точка входа
     /// </summary>
     /// <param name="args">это массив строк аргуметов</param>
        static void Main(string[] args)
        {

            Console.WriteLine("Пример работы атрибута Flags  (efoo)3    =    " + (efoo)3);
            Console.WriteLine("Указав что перечесление это флаги, меняется отображение, нужно уточнить как именно это считается \n дело похоже в побитовом сложении");


            FileActions fileActions = FileActions.ReadWrite;

            if (fileActions.HasFlag(FileActions.Write))
                Console.WriteLine("Do Write");

            if (fileActions.HasFlag(FileActions.Read))
                Console.WriteLine("do READ");




            DownloadingStatuses status = DownloadFile("fdg");

            int numberStatus = (int)status;

            DownloadingStatuses castedStatus = (DownloadingStatuses)numberStatus;

            DownloadingStatuses stringStatus = (DownloadingStatuses)Enum.Parse(typeof(DownloadingStatuses), "Error");

            Console.WriteLine(stringStatus.ToString());

            if (status == DownloadingStatuses.Downloaded)
            {

            }
            else if (status == DownloadingStatuses.Error)
            {

            }

            Console.WriteLine(new string('-', 60));


            int someRole1 = (int)(Roles.BaseUser | Roles.Operator);
            Console.WriteLine((Roles)someRole1);  //baseUser, Operator

            Roles someRole2 = Roles.BaseUser | Roles.Operator | Roles.Admin;
            Console.WriteLine(someRole2);

            if ((someRole2 & Roles.Operator) == Roles.Operator)
                Console.WriteLine($"{someRole2} содержит роль {Roles.Operator}");
            else Console.WriteLine($"{someRole2} НЕ содержит роль {Roles.Operator}");

            if ((someRole2 & Roles.SuperUser) == Roles.SuperUser)
                Console.WriteLine($"{someRole2} содержит роль {Roles.SuperUser}");
            else Console.WriteLine($"{someRole2} НЕ содержит роль {Roles.SuperUser}");

            if ((someRole2 & (Roles.BaseUser | Roles.Admin)) == (Roles.BaseUser | Roles.Admin)) //Но тут надо перепроверяться лучше так не комбинировать.
                Console.WriteLine($"{someRole2} содержит роли Roles.View | Roles.BaseUser");
            else Console.WriteLine($"{someRole2} НЕ содержит роль Roles.View | Roles.BaseUser");



        }
        /// <summary>
        /// если вернет 1 - все ок
        /// если вернет 2 - ссылка не корректна
        /// если вернет ...
        /// </summary>
        static DownloadingStatuses DownloadFile(string link)
        {
            //Console.WriteLine("dowloading...");
            return DownloadingStatuses.Downloaded;
        }
    }
    public enum DownloadingStatuses : byte
    {
        Downloaded,
        Downloding,
        Cancelled,
        Error
    }

    [Flags]
    public enum FileActions : byte
    {
        Read = 1,
        Write = 2,
        ReadWrite = Write | Read
    }

    [Flags]
    enum efoo : byte
    {
        None = 0,
        One = 1,
        Two = 2
    }
    // лучше посомтреть пример у Степана Берегового. Там про систему прав на битовых операциях куда лучше показано, и применение енумов тоже.  ключ степень двойки для полей енума.
    // вот правильный смысл битовых операций
    // скажем у нас есть необходимость завести систему прав View BaseUser Operator Dispetcher Admin SuperUser  
    // это 6 бит, следовательно маски из одного байта нам достаточно с запасом.  
    // Пусть View  это 0b000_0001
    //...
    // SuperUser это 0b0010_0000
    // работая в рамках умнажения на 2, мы можем пользоваться булевой алгеброй для задания переменных а также их комбинации, и выяснения истинны или лож сравнивая наборы переменных 
    // так же задавать большой  список прав пользуясь дешевым битовым сдвигом в цикле скажем по списку строковых констант, как пример ну или из бдэшечки
    //  View  = 0b000_0001;
    // SuperUser = View<<5;
    //Write(Convert.ToString(SuperUser, 2)); ..0b0010_0000  (64)
    // определив набор констант мы можем задать любую роль порьзуясь дизьюнкицей
    // var someRole = View | BaseUser;   получим некое двоичное число  соответвующее  набору из двух ролей
    // что бы проверить на истинность некторой роли
    //someRole & BaseUser == BaseUser
    //что бы не работать с  набором отдельных констант воспользуемся енумом, кстати если лонга 64 бита мало, то есть byteArray
    [Flags] //укажет другим разработчикам что тут перечисление олицетворяешее набор флагов.
    enum Roles : byte
    {
        None = 0b0000_00000,
        View = 1,
        BaseUser = 2,
        Operator = 4,
        Dispetcher = 0b0000_1000,
        Admin = 16,
        SuperUser = 32
    }
}
