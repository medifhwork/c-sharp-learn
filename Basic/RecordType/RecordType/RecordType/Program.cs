﻿// See https://aka.ms/new-console-template for more information

/*
Records представляют новый ссылочный тип, который появился в C#9.
Ключевая особенность records состоит в том, что они могут представлять неизменяемый (immutable) тип, который по умолчанию обладает рядом дополнительных возможностей по сравнению с классами.
Зачем нам нужны неизменяемые типы? Такие типы более безопасны в тех ситуациях, когда нам надо гарантировать, что данные объекта не будут изменяться. В .NET в принципе уже есть неизменяемые типы, например, String.
*/


/*Все records являются ссылочными типами. На уровне промежуточного языка IL, в который компилируется код C#, для record фактически создается класс.
Стоит отметить, что records являются неизменяемыми (immutable) только при определенных условиях. Например, мы можем использовать выше определенный тип Person следующим образом:*/


var person = new Person() { Name = "Tom" };
person.Name = "Bob";
Console.WriteLine(person.Name); // Bob

/*При выполнении этого кода не возникнет никакой ошибки, мы спокойно сможем изменять значения свойств объекта Person.
 * Чтобы сделать его действительно неизменяемым, надо использовать модификатор init вместо обычных сеттеров.
 */

var person2 = new PersonImutable() { Name = "Tom" };
//person2.Name = "Bob"; ошибка компиляции
Console.WriteLine(person2.Name); // Tom

Console.WriteLine(new string('-',60));
var myrecord = new Test3("One", "Two");
Console.WriteLine(myrecord.Name);
//myrecord.Name = "NonOne"; ошибка компиляции, нарушение имутабильности

// использование ключевого слова with для копирования записей
var otherPerson = person with { Name = "NewTom" };



/*Равенство двух объектов структуры записей схоже с равенством двух
 * обычных структур – равенство истинно, если эти два объекта хранят одни и те же данные:
 Стоит отметить, что для record struct не генерируется конструктор копирования.
Если определить его и использовать ключевое слово with при инициализации нового объекта, 
то вызываться будет оператор присваивания, вместо конструктора копирования
(как это происходит при работе с record class).
 */
Console.WriteLine(new string('-', 60));

var firstRecord = new Test("Nick", "Smith");
var secondRecord = new Test("Robert", "Smith");
var thirdRecord = new Test("Nick", "Smith");

Console.WriteLine(firstRecord == secondRecord);// False
Console.WriteLine(firstRecord == thirdRecord);// True


/*Во многим records похожи на обычные классы, например, они могут абстрактными,
 * их также можно наследовать либо запрещать наследование с помощью оператора sealed.
 * Тем не менее есть и ряд отличий. Рассмотрим некоторые основные отличия records от классов.*/

public record Person
{
    public string Name { get; set; }
    public int Age { get; set; }
}

public record PersonImutable
{
    public string Name { get; init; }
    public int Age { get; init; }
}
/*новвоведение 10 шарпа, реадонли структура аналогична record class c init setters*/
readonly record struct Test(string Name, string Surname); //imutible
record struct Test2(string Name, string Surname); //nonimutible
//ключевое словао class подчеркивает что запись является ссылочным типом
public record class Test3(string Name, string Surname);//imutible
