﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NullableExample
{
    class Program
    {
        static void Main(string[] args)
        {
            int? A = null;// ? добавлят интерфейс нуллабл наверно
            Nullable<int> C;
            ///int B = C;  // так уже нельзя сделать тк это разные типы. ссылочный и структурой
             A = 5;
            Console.WriteLine(nameof(A) + " = " + A);

            if  (A != null)
            {
                Console.WriteLine(A.GetType());
            }
            int B=0;


            //B = (int)A; //InvalidOperationExeption if A is null
            if (A.HasValue)  // Do It
            {
                B = A.Value;
                B = (int)A; //равнозначно но без проверки не стоит
            }
            int? Disnull = null;
            int D = Disnull ?? -1; //мега оператор ?? 
            Console.WriteLine(nameof(B)+" = "+ B);
            Console.WriteLine(nameof(D) + " = " + D);
        }


    }
}
