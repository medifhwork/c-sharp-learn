﻿using System;
using System.Linq;

namespace LogicalOperations
{
    class ParamsArgument
    {
        public static double Avg(params double[] value) => value.Sum() / value.Length;
                                                           //LinQ
        public static void FooAvg()
        {
            Console.WriteLine("передаем массив параметров, как отдельные аргументы в этом смысл params \n"
                              + "среднее чисел 3.4, 5, 6.6 = "+ Avg(3.4, 5.0, 6.6));
        }
    }
}

