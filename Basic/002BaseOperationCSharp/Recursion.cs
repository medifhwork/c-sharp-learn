﻿using System;


namespace LogicalOperations
{
    class Recursion
    {
        //Классический пример вычисления факториала
        public long Factorial(long i)
        {
            if (i < 0) throw new ArgumentOutOfRangeException(nameof(i) + " передано не корректное значение или переполнение типа");
            long result;
            if (i == 1)   //своего рода аналог условия _выхода_ в for или while, оба эти цикла заменяются рекурсией
                return 1;  // после вызова возврата значения дальшейший код недостижим
                           //базовым поинтом любого рекурсивного алгоритма является определние условия конечной итерации.
            try
            {
                checked
                {
                    result = Factorial(i - 1) * i;  //тут же и заключается отличие от классического цикла, для огранизации рекурсии вызываем сам метод,
                }
                    return result;
            }
            catch (OverflowException e)
            { Console.WriteLine(nameof(i)+" слишком большой оргумент, попробуйте умен \n"+e.Message);
            return -1;                
            }


        }
    }
}
