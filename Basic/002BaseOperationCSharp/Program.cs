﻿using System;
using LogicalOperations;
using System.Globalization;
using System.Text.Unicode;

namespace LogicalOperations
{
    class Program
    {
        static  string fieldByString;

        static void Main(string[] args)
        {
            #region less1 LogicOperations

            static bool CheckTrue()
            {
                Console.WriteLine("return true");
                return true;
            }
            static bool CheckFalse()
            {
                Console.WriteLine("return false");
                return false;
            }
             
            if (!CheckFalse() || CheckTrue()) //тут вступает в работу оптимизация, тк логически нет смысла проверять второй вызов, если первый вернул тру
            {
                Console.WriteLine("Hello World!");
            }
            int x = 12;                 // 00001100
            Console.WriteLine("x в 10 формате  = "+ x);
            Console.WriteLine("x в 2 формате = "+Convert.ToString(x, 2));
            x = ~x+1;  //  // 11110011   +1  потому что дополнительный код бла бла бла мнимый ноль что то про это
            Console.WriteLine($"{Convert.ToInt32(x.ToString(), 10).ToString()}") ;      //  -12
            Console.WriteLine($"{Convert.ToString(x,2):0,0}");

            Console.WriteLine(new string('_',120));

            //получим отрицательно число используя битовую операцию и дополнительный код +1
            int positiveN = 13;
            int negativeN = ~positiveN + 1;
            Console.WriteLine(negativeN);

            //нагляднее присвоить результат логической переменой напрямую без блоков if else сокращает запись и улучшает читаемость
            int a = 5; int b = 0;
            bool res = a > b;
            Console.WriteLine(nameof(res)+" = "+ res);

            //тернарный оператор ?

            bool res2 = a > b ? true : false;  //можно но такая запись избыточна сама по себе
            string res3 = a > b ? "a > b" : "b > a"; //а вот эта уже имеет смысл и сокрашает запись
            Console.WriteLine(nameof(res2) + " = " + res2);
            Console.WriteLine(res3);

            // оператор проверки на null -  ??   есть ораничеение типа должны быть консистенты, и левый операнд должен быть Nullable

            int? nullablValue = null; //для примера, можно было и обьект, кароч ссылочное что ни будь
            int res4 = nullablValue ?? -100; //-100
            Console.WriteLine($"res4 = {res4} and nullableVaue = '{nullablValue}'"); //вывод в консоль нулл заменяет пусто строкой
            int? nullablValue2 = 100; 
            int res5 = nullablValue2 ?? -200; //100
            Console.WriteLine($"res5 = {res5} and nullableVaue = '{nullablValue2}'"); //вывод в консоль нулл заменяет пусто строкой

            // оператор .? безопасного разименования указателя

            Human dobrynia = new Human();
            dobrynia?.walking();  // все как обычно
            dobrynia = null; // познакомим Добрыню с пустотой
            dobrynia?.walking();  // не гуляет больше Добрыня. Кощей ручки потирает и хихакает злобно.

            Console.WriteLine(new string('=',120));

                byte num = 0xA;  //16 base
                byte num2 = 0b00001010;  //2 base
                byte num3 = 0b0000_1010;

                Console.WriteLine("num = {0}",num);
                Console.WriteLine("num2 = {0}",num2);
                Console.WriteLine("num3 = {0}",num3);

            Console.WriteLine(new string('=', 120));
            Console.WriteLine("Последовательность вычисления аргументов функции Sum");
            int Sum(int a, int b) => a + b;
            int aaa = 1;
            int bbb = 2;
            var ccc = Sum(aaa++,aaa+bbb); //5 вычисление операвторов происходить слева на право, в функцию передается ааа, затем инкрементируется тк пост ++, после чего измененная ааа складывается с ббб и передается вторым аргументом
             ccc = Sum(aaa++,aaa+bbb); //7
             ccc = Sum(aaa++,aaa+bbb); //9  грязная фунция надо помнить что в агрументе не надо перезаписывать переменную
            Console.WriteLine(ccc);

            //Рекурсия
            Recursion recursion = new Recursion();
            Console.WriteLine("factorial -  "+ recursion.Factorial(30));

            //Params how it works
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Params how it works");
            Console.WriteLine("передаем массив параметров, как отдельные аргументы в этом смысл params \n"
                             + "среднее чисел 3.4, 5, 6.6 = " + ParamsArgument.Avg(3.4, 5.0, 6.6, 5,6,78,9.455445));
            ParamsArgument.FooAvg();
            Console.WriteLine();
            Console.WriteLine();

            
            #endregion less1


            #region less3 STRING
            
            //unicode запись фомрат символа и вывод
            Console.WriteLine("one char - " + '\u005F');
            Console.WriteLine("one char - " + '_');

            //приведение - неявное char->int
            //на этом построенные некоторые хитрые алгоритмы и это неявное приведение стоит запомнить, что бы понимать такие  алгоритмы - хаки
            int charInInt_A = 'A';
            //char charInChar_A = charInInt_A; missing cast в обратном направлении не работает
            //explcit cast
            char charInChar_A = (char)charInInt_A; 

            for (char c = 'A'; c <= 'z'; c++)
            {
                Console.Write((char)(c));
            }
            Console.WriteLine();

            // можно узнать является ли наш символ членом какой ни будь последовательности символов
            bool IsRussianLetter(char value)
            {
                return Char.ToUpper(value) >= 'А' && Char.ToUpper(value) <= 'Я';
            }

            Console.WriteLine(IsRussianLetter('5'));
            Console.WriteLine(IsRussianLetter('Й'));
            Console.WriteLine(IsRussianLetter('Q'));
            Console.WriteLine("First line" + Environment.NewLine + "Second Line " + Environment.MachineName + "\a");

            // задать локаль для текущего приложения, а также culture можно передать в перегруженный метод
            Console.WriteLine(); Console.WriteLine();

            Console.WriteLine(CultureInfo.CurrentCulture);
            Console.WriteLine(DateTime.Now);

            CultureInfo culture = new CultureInfo("ja-JP");
            CultureInfo.DefaultThreadCurrentCulture = culture;

            Console.WriteLine(CultureInfo.CurrentCulture);
            Console.WriteLine(DateTime.Now);

            //Unicode это более сложная система данных о символах чем простая таблица ANCII и др,  
            //в ней есть дополнительная информация о символах.
            void ShowCharinfo(params char[] chars)
            {
                foreach (var item in chars)
                {
                    Console.WriteLine(new String('-', 60)); //хороший кейс явного конструктора стринги имхо
                    Console.WriteLine("Simbol " + item + " IsControl - " + char.IsControl(item));
                    Console.WriteLine("Simbol " + item + " IsWhiteSpace - " + char.IsWhiteSpace(item));
                    Console.WriteLine("Simbol " + item + " IsPunctuation - " + char.IsPunctuation(item));
                    Console.WriteLine("Simbol " + item + " IsSeparator - " + char.IsSeparator(item));
                    Console.WriteLine("Simbol " + item + " IsSymbol - " + char.IsSymbol(item));
                    Console.WriteLine("Simbol " + item + " IsNumber - " + char.IsNumber(item));
                    Console.WriteLine("Simbol " + item + " IsDigit - " + char.IsDigit(item));
                    Console.WriteLine("Simbol " + item + " IsLetter - " + char.IsLetter(item));
                    Console.WriteLine("Simbol " + item + " IsSymbol - " + char.IsSymbol(item));
                    Console.WriteLine("Simbol " + item + " IsUpper - " + char.IsUpper(item));
                }
            }
            ShowCharinfo('①', '¹', '⅓', '४', '六');

            //Ининцилизация строк
            var S1 = new String('-', 60);
            S1 = "dsf";
            S1 = String.Empty;
            string S2;// пустая строка в куче
            //var resEquals = (S1 == S2); не выйдет S2 не ининцилизированна, копмилятор по рукам
            // то есть строка "" != null
            var resEquals = Object.ReferenceEquals(S1, fieldByString);
            Console.WriteLine("S1 равны ли по ссылке с fieldByString :" + resEquals);
            Console.WriteLine("S1 == fieldByString :" + ((bool)(S1 == fieldByString)).ToString());
            Console.WriteLine("S1.Equals(fieldByString): " + S1.Equals(fieldByString));

            char[] chars = "dfdsretsgsdfgergtregfdfg#".ToCharArray();

            string S3 = new string(chars);  //такая вот тема.


            // интернирование - сокрашение затрат памяти путем резервирования зарание определнных строк в спец таблице
            //Смысл в том, что если по содержанию такие строки равыны, то они будут иметь одинаковую ссылку

            string si1 = "Hel lo";
            string si2 = si1.Replace(' ', '_');
            si1.Replace(' ', '_');  //сама строка не изменна
            Console.WriteLine(si1);
            Console.WriteLine(si2);
            string si3 = "Hel lo";
            Console.WriteLine("Обрати внемание переменные разные, но по ссылке они равны, так работает интернирование на момент компиляции \n"
                + Object.ReferenceEquals(si1, si3));
            //string si3_1 = "Hel l"+ new string('o',1);
            //Console.WriteLine("А сейчас они не равны, хотя по содержанию одинаковы"+
            //    "тк вторая срока собрана в рантайме \n"+object.ReferenceEquals(si3,si3_1));
            string si3_1 = String.Intern(si1); //тут немного не понятно, чем интернирование отличается от установки указателя в др языках
            Console.WriteLine("Сейчас сделано принудительное интернирование \n"
                + object.ReferenceEquals(si3, si3_1));
            Console.WriteLine(si3_1);

            //стринга эффективна до 6 склеек слов, дальше Stringbilder выгоднее испоьзовать
            //У стрингбилдера есть важное свойство Capacity 
            //под копотом массив чаров, увеличивающийся в двое начиная с 16 элементов массива.
            //16 32 64  итд
            // можно этим играться управлять при необходимости оптимизации
            //MaxCapacity 

            string sExplicity = @"C:\Users\Medifh\Desktop\1.txt";
            //@ всего лишь отключает EsCAPe последовательности

            #endregion less3 string


            #region less3 StringFormat

            //Параметры форматирования
            //C / c
            //Задает формат денежной единицы, указывает количество десятичных разрядов после запятой
            //D / d
            //Целочисленный формат, указывает минимальное количество цифр
            //E / e
            //Экспоненциальное представление числа, указывает количество десятичных разрядов после запятой
            //F / f
            //Формат дробных чисел с фиксированной точкой, указывает количество десятичных разрядов после запятой
            //G / g
            //Задает более короткий из двух форматов: F или E
            //N / n
            //Также задает формат дробных чисел с фиксированной точкой, определяет количество разрядов после запятой
            //P / p
            //Задает отображения знака процентов рядом с число, указывает количество десятичных разрядов после запятой
            //X / x

            
            Console.WriteLine(new String('=', 60));
            Console.WriteLine("Стринг форматинг");
            Console.WriteLine(new String('=', 60));
            culture = new CultureInfo("ru-RU");
            CultureInfo.DefaultThreadCurrentCulture = culture;

            Console.WriteLine("форматинг валюты С");
            double number = 23.7;
            string result = String.Format("{0:C}", number);  //культура очень влияет на вывод и округление, для япноии ок 23.7 ~ 24
            Console.WriteLine(result); // $ 23.7
            Console.WriteLine($"{number:c3}"); // $ 23.7
            string result2 = String.Format("{0:C2}", number);
            Console.WriteLine(result2); // $ 23.70



            Console.WriteLine("форматинг целых чисел d");
            int number23 = 23;
            string result23 = String.Format("{0:d}", number23);
            Console.WriteLine(result23); // 23
            string result223 = String.Format("{0:d4}", number23);
            Console.WriteLine(result223); // 0023
            Console.WriteLine($"{number23:d6}"); // 0023


            Console.WriteLine("форматинг вещественных чисел F");
            int myn = 23;
            string res321 = String.Format("{0:f}", myn);
            Console.WriteLine(res321); // 23,00

            double myn2 = 45.08;
            string res2321 = String.Format("{0:f4}", myn2);
            Console.WriteLine(res2321); // 45,0800

            double myn3 = 25.07;
            string res3321 = String.Format("{0:f1}", myn3);
            Console.WriteLine(res3321); // 25,1


            Console.WriteLine("\n\n\n Настраиваемое форматирование");
            long kukummber = 79120257626;
            string resultumber = String.Format("{0:+# (###) ###-##-##}", kukummber);
            Console.WriteLine(resultumber); // +7 (912) 025-76-026
            Console.WriteLine($"{kukummber:+#(###)###-##-##}");
            double iii1 = 32.345;
            double iii2 = 0;
            double iii3 = -32.345;
            Console.WriteLine($"{iii1:#0.0#;(#0.0#);-ZERO-}");// ;Определяет секции с раздельными строками формата для положительных чисел, отрицательных чисел и нуля.
            Console.WriteLine($"{iii2:#0.0#;(#0.0#);-ZERO-}");
            Console.WriteLine($"{iii3:#0.0#;(#0.0#);-ZERO-}");


            //группы
            double value = 1234567890;
            Console.WriteLine(value.ToString("#,#", CultureInfo.InvariantCulture));
            Console.WriteLine(String.Format(CultureInfo.InvariantCulture,
                                            "{0:#,#}", value));

            //проценты
            double valy = .086;
            Console.WriteLine(valy.ToString("#0.##%", CultureInfo.InvariantCulture)); //прикольная перегрузка контруктора стринги
            Console.WriteLine($"{0.97m:#0.##%}");
            // Displays 8.6%

            //TODO поработать с датами.


            


            #endregion less3 StringFormat


            #region Lesson 4 Garbage Collector
            
            MyClass me = new MyClass();
           
           
            #endregion Lesson 4 Garbage Collector

        }
    }

    #region Lesson 4 Garbage Collector support object

    public class MyClass
    {
        //Destructor
        ~MyClass() {
            for (int i = 0; i < 40; i++)
            {
                Console.WriteLine(i);
                System.Threading.Thread.Sleep(1000);
            }           
        }    
    }



    #endregion Lesson 4 Garbage Collector  support object

    #region other
    public class Human
    {
        public void walking()
        {
            Console.WriteLine("i am walking...");
        }

    }  

    #endregion

}
