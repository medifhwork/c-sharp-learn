﻿using System;

namespace StructExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Point p1 = new Point(x:1, y:2); ; ; ; ; ; ; ; ; ;
            Console.WriteLine($"P = {p1.X};{p1.Y}");
            Point p2 = new Point(x: 10, y: 20);
            Console.WriteLine($"Point p2 = new Point(x: 10, y: 20)");
            Console.WriteLine($"P2 equal P1 is : {p1.Equals(p2)}");
            Console.WriteLine("p2 = p1");
            p2 = p1;
            Console.WriteLine($"P2 equal P1 is : {p1.Equals(p2)}");
            Console.WriteLine("But p1.Y = 30;");
            p1.Y = 30;
            Console.WriteLine($"P2 = {p2.X};{p2.Y}");
            Console.WriteLine($"P = {p1.X};{p1.Y}");//ведет себя как обычная переменная, тк не является ссылочным типом!
            Console.WriteLine($"P2 equal P1 is : {p1.Equals(p2)}");
        }

        public struct Point  //все поля структы в момент создания иницилизируются закрытым конструктором по умолчанию!
                             //Значения дефолтные для базовых типов.
        {
            public int X;
            public int Y; 

            public Point(int x, int y) //если определил конструктор явно, все поля должны быть иницилизированны, это главное отличие от класса
            {
                X = x;
                Y = y;
            }
        }
    }
}
