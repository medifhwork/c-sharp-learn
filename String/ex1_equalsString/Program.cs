﻿using System;

namespace StringInternHabr
{
    class Program
    {
        static void Main(string[] args)
        {
            string a = "hello world";
            string b = a;
            a = "hello";
            Console.WriteLine("{0}, {1}", a, b);
            Console.WriteLine(a == b);
            Console.WriteLine(object.ReferenceEquals(a, b));
            Console.WriteLine((a + " world") == b);
            Console.WriteLine(object.ReferenceEquals((a + " world"), b));
            Console.WriteLine(b.Equals(a));
            Console.WriteLine(b.Equals(a + " world"));

        }

    }
}
