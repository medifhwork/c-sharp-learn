﻿using System;

class Program
{
    static void Main(string[] args)
    {
        string hello = "hello";
        string helloWorld = "hello world";
        string helloWorld2 = hello + " world";

        Console.WriteLine("{0}, {1}: {2}, {3}", helloWorld, helloWorld2,
            helloWorld == helloWorld2,
            object.ReferenceEquals(helloWorld, helloWorld2));

        //Console.WriteLine(object.ReferenceEquals(
        //    String.Intern(helloWorld),
        //    String.Intern(helloWorld2)));

        helloWorld2 = "hello world";
        Console.WriteLine("{0}, {1}: {2}, {3}", helloWorld, helloWorld2,
            helloWorld == helloWorld2,
            object.ReferenceEquals(helloWorld, helloWorld2));

        Console.WriteLine(new string('-',60));

        string a = new string(new char[] { 'a', 'b', 'c' });
        object o = String.Copy(a); 
        Console.WriteLine(object.ReferenceEquals(o, a)); //false
        String.Intern(o.ToString());
        Console.WriteLine(object.ReferenceEquals(o, String.Intern(a))); //true

        object o2 = String.Copy(a);
        String.Intern(o2.ToString());
        //o2=String.Intern(o2.ToString());  //если сделать так то вернет тру, тк вытащив ссылку на значение абс перезапишет ее для о2
        Console.WriteLine(object.ReferenceEquals(o2, String.Intern(a))); //false

        string str = "qqq"+new char[] {'s'};
        string ostr = String.IsInterned(str) ?? "not interned";
        Console.WriteLine(ostr);

        Console.WriteLine(new string('-', 60));

        string s = new string(new char[] { 'x', 'y', 'z' });
        Console.WriteLine(String.IsInterned(s) ?? "not interned");
        String.Intern(s);
        Console.WriteLine(String.IsInterned(s) ?? "not interned");
        Console.WriteLine(object.ReferenceEquals(
        String.IsInterned(new string(new char[] { 'x', 'y', 'z' })), s));

        //Console.WriteLine(object.ReferenceEquals("xyz", s));  //причина в литерале xyz, на этапе копиляции вся логика  написанная выше изменится тк литерал уже в пуле интернирования
        //Console.WriteLine(object.ReferenceEquals("x" + "y" + "z", s));// тоже литерал тот же самый



    }
}