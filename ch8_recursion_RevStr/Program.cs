﻿using System;
using System.Text;

namespace ch8_recursion_RevStr
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var resStr = new ResStr();
            resStr.DisplayRev("1234");
        }

    }

    class ResStr
    {
        public void DisplayRev(string str)
        {
            if (str.Length > 0)
            {
                DisplayRev(str.Substring(0, str.Length - 1)); //поменяй 22 и 23 строки местами, объясни почему в одном случае вывод 1234 в другом будет 4321
                Console.Write(str[str.Length-1]);                
            }
            else
            Console.Write("");
        }
    }
}
